/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   DerivedB.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 08:51:29 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 13:12:18 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DERIVEDB_HPP
#define DERIVEDB_HPP

#include "Base.hpp"

class DerivedB : public Base
{
public:
    ~DerivedB();
};

#endif /* DERIVEDB_HPP */
