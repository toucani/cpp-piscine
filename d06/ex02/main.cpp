/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 08:55:02 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 13:12:21 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "DerivedA.hpp"
#include "DerivedB.hpp"
#include "DerivedC.hpp"
#include <cstdlib>
#include <iostream>

static Base *generate()
{
    std::cout << "Generating new class... ";
    Base *result;
    switch(rand() % 3)
    {
        case 0:
            std::cout << "A: ";
            result =  new DerivedA();
            break;
        case 1:
            std::cout << "B: ";
            result =  new DerivedB();
            break;
        case 2:
            std::cout << "C: ";
            result = new DerivedC();
            break;
        default:
            result = 0;
    }
    std::cout << result << std::endl;
    return result;
}

static void identify_from_pointer(Base *b)
{
    if (dynamic_cast<DerivedA*>(b))
        std::cout << b << " is A!" << std::endl;
    else if (dynamic_cast<DerivedB*>(b))
        std::cout << b << " is B!" << std::endl;
    else if (dynamic_cast<DerivedC*>(b))
        std::cout << b << " is C!" << std::endl;
}

static void identify_from_reference(Base &b)
{
    try
    {
        DerivedA &a = dynamic_cast<DerivedA&>(b);
        std::cout << &b << " is A(" << &a << ")!" << std::endl;
        return;
    }
    catch(std::bad_cast& e) {}
    try
    {
        DerivedB &a = dynamic_cast<DerivedB&>(b);
        std::cout << &b << " is B(" << &a << ")!" << std::endl;
        return;
    }
    catch(std::bad_cast& e) {}
    try
    {
        DerivedC &a = dynamic_cast<DerivedC&>(b);
        std::cout << &b << " is C(" << &a << ")!" << std::endl;
        return;
    }
    catch(std::bad_cast& e) {}
}

int main()
{
    srand(time(0));
    for(int i = 0; i < 3; i++)
    {
        Base *b = generate();
        identify_from_pointer(b);
        identify_from_reference(*b);
        std::cout << std::endl;
    }
    return 0;
}
