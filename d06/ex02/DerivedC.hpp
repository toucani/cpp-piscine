/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   DerivedC.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 08:51:29 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 13:12:20 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DERIVEDC_HPP
#define DERIVEDC_HPP

#include "Base.hpp"

class DerivedC : public Base
{
public:
    ~DerivedC();
};

#endif /* DERIVEDC_HPP */
