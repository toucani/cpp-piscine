/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   DerivedA.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 08:51:29 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 13:12:16 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DERIVEDA_HPP
#define DERIVEDA_HPP

#include "Base.hpp"

class DerivedA : public Base
{
public:
    ~DerivedA();
};

#endif /* DERIVEDA_HPP */
