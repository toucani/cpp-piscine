/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 08:48:40 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 13:12:03 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Converter.hpp"
#include <iostream>

int main(int argc, char** argv)
{
    if (argc == 1)
    {
        std::cout << "So... Where are the arguments?" << std::endl;
        return 0;
    }

    for (int i = 1; i < argc; i++)
    {
        std::cout << Converter(argv[i]);
    }

    return 0;
}
