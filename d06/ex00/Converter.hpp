/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Converter.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 08:48:36 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 13:12:01 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONVERTER_HPP
#define CONVERTER_HPP

#include <string>
#include <ostream>

class Converter
{
public:
    Converter();
    Converter(const char*);
    Converter(const Converter&);
    ~Converter();
    std::string toStr() const;
    Converter& operator=(const Converter&);
private:
    char _valueC;
    int _valueI;
    float _valueF;
    double _valueD;
};

std::ostream& operator<<(std::ostream&, const Converter&);

#endif /* CONVERTER_HPP */
