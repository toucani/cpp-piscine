/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Converter.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 08:48:32 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/11 17:55:37 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Converter.hpp"
#include <sstream>
#include <math.h>

Converter::Converter() : _valueC(0), _valueI(0), _valueF(0), _valueD(0) {}
Converter::Converter(const char* arg)
{
    if (arg[1] == '\0' && std::isalpha(arg[0]))
    {
        _valueC = arg[0];
        _valueI = static_cast<int>(_valueC);
        _valueF = static_cast<float>(_valueC);
        _valueD = static_cast<double>(_valueC);
    }
    else
    {
        _valueD = std::atof(arg);
        _valueF = static_cast<float>(_valueD);
        _valueI = static_cast<int>(_valueD);
        _valueC = static_cast<char>(_valueD);
    }
}

Converter::Converter(const Converter& other) { *this = other; }
Converter::~Converter() {}
std::string Converter::toStr() const
{
    std::stringstream ss;
    ss << "Char:    ";
    if (std::isprint(_valueC))
        ss << _valueC;
    else
        ss << "Non printable";

    ss << "\nInt:    ";
    if (isnan(_valueD) || isnan(_valueF) || isinf(_valueD) || isinf(_valueF))
        ss << "Non printable";
    else
        ss << _valueI;

    ss << "\nFloat:  " << _valueF << "\n";
    ss << "Double: " << _valueD;
    return ss.str();
}

Converter& Converter::operator=(const Converter& other)
{
    _valueC = other._valueC;
    _valueI = other._valueI;
    _valueD = other._valueD;
    _valueF = other._valueF;
    return *this;
}

std::ostream& operator<<(std::ostream& os, const Converter& c)
{
    os << c.toStr() << std::endl;
    return os;
}
