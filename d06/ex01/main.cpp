/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 10:07:05 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/12 15:01:40 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cstdlib>
#include <iostream>
#include <cstring>

struct Data
{
    std::string s1;
    std::string s2;
    int n;
};

void *serialize()
{
    const size_t size = 16 + sizeof(int);
    void* mem = reinterpret_cast<void*>(new(char[size]));
    for (unsigned i = 0 ; i < size; i++)
        //Just to produce printable chars
        *(reinterpret_cast<char*>(mem) + i) = (rand() % 90) + 34;
    return mem;
}

Data* deserialize(void* r)
{
    char line[2][9] = { {}, {} };
    const char *raw = reinterpret_cast<const char*>(r);

    //Copying the first raw string
    std::memcpy(line[0], r, 8);
    //Moving the pointer itself
    r = reinterpret_cast<void*>(const_cast<char*>(raw + 8 + sizeof(int)));
    std::memcpy(line[1], r, 8);//Copying the second raw string

    Data *data = new Data();
    data->n = *(reinterpret_cast<const int*>(raw + 8));
    data->s1 = std::string(reinterpret_cast<const char*>(line[0]));
    data->s2 = std::string(reinterpret_cast<const char*>(line[1]));
    return data;
}

int main()
{
    srand(time(0));

    for(int i = 0; i < 3; i++)
    {
        void *mem = serialize();
        Data *data = deserialize(mem);
        std::cout << "Raw memory: ";
        for (unsigned i = 0; i < 16 + sizeof(int); i++)
            std::cout << (reinterpret_cast<char*>(mem))[i];
        std::cout << std::endl;
        std::cout << "Data:\n\tS1: " << data->s1 << "\n\tS2: "
            << data->s2 << "\n\t N: " << data->n << "\n" << std::endl;
        delete data;
    }

    return 0;
}
