/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 09:36:06 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/03 09:37:34 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"
#include <sstream>

Human::Human()
    : brain(Brain())
{
}

std::string Human::identify() const
{
    std::stringstream ss;
    ss << static_cast<const void*>(&brain);
    return ss.str();
}

const Brain &Human::getBrain() const
{
    return brain;
}
