/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 08:50:32 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/04 12:27:41 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include <iostream>
#include <stdlib.h>
#include <time.h>

const std::string zombiePhrase = "Braiiiiiiinnnssss...";

Zombie::Zombie()
	: _type("Default type")
{
	_name = GetRandomName();
}

Zombie::Zombie(const std::string& name, const std::string& type)
	: _name(name), _type(type)
{
}

void Zombie::announce() const
{
	std::cout << _name << "(" << _type << ") " << zombiePhrase << std::endl;
}

void Zombie::SetType(const std::string& type)
{
	_type = type;
}

const std::string& Zombie::GetType() const
{
	return _type;
}

std::string Zombie::GetRandomName()
{
	//Minimum 2, max - 7
    const unsigned nameLength = (rand() % 5) + 2;
    std::string name;
    for (unsigned ct = 0; ct < nameLength; ct++)
    {
        name += 'a' + rand() % 25;
    }
    //Uppercasing it
    name[0] = name[0] & '_';

    return name;
}