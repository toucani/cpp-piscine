/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 08:50:52 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/04 12:29:40 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.hpp"
#include <stdlib.h>
#include <time.h>

int main()
{
    srand(time(NULL));
    ZombieHorde zmbHorde(12);
    zmbHorde.announce();
    
    return 0;
}
