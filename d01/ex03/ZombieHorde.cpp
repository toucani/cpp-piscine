/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 08:51:02 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/04 12:20:40 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(size_t amount)
    : _amount(amount)
{
    _zombies = new Zombie[amount];
}

void ZombieHorde::announce() const
{
    for (size_t ct = 0; ct < _amount; ct++)
        _zombies[ct].announce();
}

ZombieHorde::~ZombieHorde()
{
    delete[] _zombies;
}
