/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 08:50:27 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/04 12:25:48 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_HPP
#define ZOMBIE_HPP

#include <string>

class Zombie
{
public:
	Zombie();
	Zombie(const std::string& name, const std::string& type);
	void announce() const;
	void SetType(const std::string& type);
	const std::string& GetType() const;

private:
	static std::string GetRandomName();
	
	std::string _name;
	std::string _type;
};

#endif /* ZOMBIE_HPP */
