/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 08:51:05 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/04 12:20:33 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIEHORDE_HPP
#define ZOMBIEHORDE_HPP

#include "Zombie.hpp"

class ZombieHorde
{
public:
    ZombieHorde(size_t amount);
    void announce() const;
    ~ZombieHorde();

private:
    size_t _amount;
    Zombie *_zombies;
};

#endif /* ZOMBIEHORDE_HPP */
