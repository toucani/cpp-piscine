/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 08:50:38 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/04 12:24:34 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieEvent.hpp"
#include <stdlib.h>
#include <time.h>

void ZombieEvent::setZombieType(const std::string& type)
{
    _type = type;
}

const std::string& ZombieEvent::getZombieType() const
{
    return _type;
}

Zombie* ZombieEvent::newZombie(const std::string& name)
{
    return new Zombie(name, _type);
}

void ZombieEvent::randomChump()
{
    Zombie zmb;
    zmb.SetType(_type);
    zmb.announce();
}
