/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 08:50:35 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/04 12:24:16 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIEEVENT_HPP
#define ZOMBIEEVENT_HPP

#include "Zombie.hpp"

class ZombieEvent
{
public:
    void setZombieType(const std::string& type);
    const std::string& getZombieType() const;
    Zombie* newZombie(const std::string& name);
    void randomChump();

private:
    std::string _type;
};

#endif /* ZOMBIEEVENT_HPP */
