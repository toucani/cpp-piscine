/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 08:50:24 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/04 12:28:18 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieEvent.hpp"
#include <stdlib.h>
#include <time.h>

const unsigned magicNumber = 6;

int main()
{
	srand(time(NULL));
	ZombieEvent zmbEvent;
	zmbEvent.setZombieType("stack zombie");

	for (unsigned ct = 0; ct < magicNumber; ct++)
	{
		zmbEvent.randomChump();
	}

	Zombie *zmbs[magicNumber];
	zmbEvent.setZombieType("heap zombie");
	for (unsigned ct = 0; ct < magicNumber; ct++)
	{
		zmbs[ct] = zmbEvent.newZombie("Stranger");
		zmbs[ct]->announce();
	}

	for (unsigned ct = 0; ct < magicNumber; ct++)
	{
		delete zmbs[ct];
	}

	return 0;
}
