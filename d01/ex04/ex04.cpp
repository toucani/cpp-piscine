/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 08:54:02 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/03 09:35:10 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <string>

int main()
{
	std::string text("HI THIS IS BRAIN");
	std::string *ptext = &text;
	std::string &rtext = text;
	std::cout << "Original:  " << text << std::endl;
	std::cout << "Pointer:   " << *ptext << std::endl;
	std::cout << "Reference: " << rtext << std::endl;
	return 0;
}
