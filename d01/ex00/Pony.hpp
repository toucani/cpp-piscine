/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 08:43:55 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/04 12:04:26 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PONY_HPP
#define PONY_HPP

#include <string>

class Pony
{
public:
    Pony(const std::string &name = "Default pony name",
        const std::string &color = "black",
        const std::string &mood = "happy",
        float weight = 100.f);
    ~Pony();
    void makeSound() const;

private:
    std::string _name;
    std::string _color;
    std::string _mood;
    float _weight;
};

#endif /* PONY_HPP */