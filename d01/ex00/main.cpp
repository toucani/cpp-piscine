/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 08:43:46 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/03 19:07:53 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Pony.hpp"

static void message(const bool st = true)
{
    std::cout << std::endl;
    std::cout << "Creating pony on the " << (st ? "stack" : "heap") << ":" << std::endl;
    std::cout << "---------------------------" << std::endl;
}

static void ponyOnTheStack()
{
    message();
    Pony pony("Dummy", "brown", "depressed", 187.9);
    pony.makeSound();
}

static void ponyOnTheHeap()
{
    message(false);
    Pony *pony = new Pony("Heaper", "pink", "happy", 135);
    pony->makeSound();
    delete pony;
}

int main()
{
    ponyOnTheStack();
    ponyOnTheHeap();
    return 0;
}
