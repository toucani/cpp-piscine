/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 08:43:51 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/03 08:43:52 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Pony.hpp"

Pony::Pony(const std::string &name, const std::string &color,
        const std::string &mood, float weight)
        : _name(name), _color(color), _mood(mood), _weight(weight)
{
    std::cout << _name << ", a " << _weight << "kg "<< _color << " pony, in "
    << _mood << " mood is created." << std::endl;
}

Pony::~Pony()
{
    std::cout << _name << ", a " << _weight << "kg "<< _color << " pony, in "
    << _mood << " mood is destroyed." << std::endl;
}

void Pony::makeSound() const
{
    std::cout << _name << " says: Hee-Haw!" << std::endl;
}
