/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/11 09:23:06 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/11 17:42:34 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Span.hpp"
#include <iostream>

static void SubjectTest()
{
    Span sp = Span(5);
    sp.addNumber(5);
    sp.addNumber(3);
    sp.addNumber(17);
    sp.addNumber(9);
    sp.addNumber(11);
    std::cout << sp.shortestSpan() << std::endl;
    std::cout << sp.longestSpan() << std::endl;
}

int main()
{
    SubjectTest();
    std::cout << std::endl;

    Span s(10);

    try
    {
        s.longestSpan();
    }
    catch (std::exception& e)
    {
        std::cout << "WAT??? Error: " << e.what() << std::endl;
    }

    for (int i = 0; i < 10; i++)
    {
        s.addNumber(i * i);
    }

    try
    {
        std::cout << s;
        std::cout << "Longest span: " << s.longestSpan()
            << "\nShortest span: " << s.shortestSpan() << std::endl;
    }
    catch (std::exception& e)
    {
        std::cout << "WAT??? Error: " << e.what() << std::endl;
    }

    return 0;
}
