/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Span.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/11 09:23:48 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/11 17:33:17 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPAN_HPP
#define SPAN_HPP

#include <iterator>
#include <vector>
#include <ostream>

class Span
{
public:
    Span();
    Span(unsigned);
    Span(const Span&);
    ~Span();

    void addNumber(int);
    void addNumbers(std::vector<int>::iterator begin,
                    const std::vector<int>::iterator& end);

    int shortestSpan() const;
    int longestSpan() const;
    void Print(std::ostream&) const;
    Span& operator=(const Span&);

private:
    std::vector<int> _data;
    unsigned _current;
};

std::ostream& operator<<(std::ostream&, const Span&);

#endif /* SPAN_HPP */
