/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Span.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/11 09:30:40 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/11 17:43:01 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Span.hpp"
#include <stdexcept>
#include <algorithm>
#include <numeric>

Span::Span() {}
Span::Span(unsigned size) { _data.resize(size, 0); _current = 0; }
Span::Span(const Span& other) { *this = other; }
Span::~Span() {}

void Span::addNumber(int value)
{
    if (_current >= _data.size())
        throw std::length_error("Container is full.");

    _data[_current++] = value;
}

void Span::addNumbers(std::vector<int>::iterator begin,
                    const std::vector<int>::iterator& end)
{
    for (; begin != end; begin++)
    {
        addNumber(*begin);
    }
}

int Span::shortestSpan() const
{
    if (_current <= 1)
        throw std::logic_error("Too few elements to compute span!");

    std::vector<int> result(_data.size());
    std::adjacent_difference(_data.begin(), _data.end(), result.begin());
    std::sort(result.begin(), result.end());
    return std::abs(result[1]);
}

int Span::longestSpan() const
{
    if (_current <= 1)
        throw std::logic_error("Too few elements to compute span!");

    std::vector<int> result(_data.size());
    std::adjacent_difference(_data.begin(), _data.end(), result.begin());
    std::sort(result.begin(), result.end());
    return std::abs(result.back());
}

void Span::Print(std::ostream& os) const
{
    for (size_t ct = 0; ct < _data.size(); ct++)
        os << _data[ct] << " ";
    os << std::endl;
}

Span& Span::operator=(const Span& other)
{
    _data = other._data;
    _current = other._current;
    return *this;
}

std::ostream& operator<<(std::ostream& os, const Span& s)
{
    s.Print(os);
    return os;
}