/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   easyfind.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/11 08:53:34 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/11 09:16:28 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EASYFIND_HPP
#define EASYFIND_HPP

#include <algorithm>

template <typename T>
const int* easyfind(const T& container, int value)
{
    typename T::const_iterator it = std::find(container.begin(), container.end(), value);
    return it != container.end() ? &(*it) : 0;
}

template <typename T>
int* easyfind(T& container, int value)
{
    typename T::iterator it = std::find(container.begin(), container.end(), value);
    return it != container.end() ? &(*it) : 0;
}

#endif /* EASYFIND_HPP */
