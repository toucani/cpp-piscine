/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/11 08:53:39 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/11 09:16:44 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "easyfind.hpp"
#include <vector>
#include <list>
#include <iostream>

template <typename T>
static void find(T& c, int value)
{
    const int *found = easyfind(c, value);
    if (found)
        std::cout << "Found: " << value << " = " << *found << std::endl;
    else
        std::cout  << "Not found " << value << std::endl;
}

int main()
{
    std::vector<int> vec(10);
    for (int i = 0; i < 10; i++)
        vec[i] = i;

    find(vec, 0);
    find(vec, 7);
    find(vec, 9);
    find(vec, 50);
    std::endl;
    const std::list<int> list(5, 5);
    find(list, 5);
    find(list, 50);

    return 0;
}
