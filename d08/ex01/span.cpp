/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/11 09:30:40 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/11 09:58:59 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Span.hpp"
#include <stdexcept>
#include <algorithm>

Span::Span() {}
Span::Span(unsigned size) { _data.resize(size, 0); _current = 0; }
Span::Span(const Span& other) { *this = other; }
Span::~Span() {}

void Span::addNumber(int value)
{
    if (_current >= _data.size())
        throw std::length_error("Container is full.");

    _data[_current++] = value;
}

void Span::addNumbers(std::iterator<std::input_iterator_tag, int> begin,
                    const std::iterator<std::input_iterator_tag, int>& end)
{
    for (; begin != end; begin++)
    {
        addNumber(*begin);
    }
}

int Span::shortestSpan() const
{
    if (_current <= 1)
        throw std::logic_error("Too few elements to compute span!");

    return 0;
}

int Span::longestSpan() const
{
    if (_current <= 1)
        throw std::logic_error("Too few elements to compute span!");

    return 0;
}

Span& Span::operator=(const Span& other)
{
    _data = other._data;
    _current = other._current;
    return *this;
}
