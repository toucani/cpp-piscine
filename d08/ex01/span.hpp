/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/11 09:23:48 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/11 09:58:10 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPAN_HPP
#define SPAN_HPP

#include <iterator>
#include <vector>

class Span
{
public:
    Span();
    Span(unsigned);
    Span(const Span&);
    ~Span();

    void addNumber(int);
    void addNumbers(std::iterator<std::input_iterator_tag, int> begin,
                    const std::iterator<std::input_iterator_tag, int>& end);

    int shortestSpan() const;
    int longestSpan() const;

    Span& operator=(const Span&);

private:
    std::vector<int> _data;
    unsigned _current;
};

#endif /* SPAN_HPP */
