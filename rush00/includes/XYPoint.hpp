/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   XYPoint.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: imarakho <imarakho@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 22:41:48 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 15:58:28 by imarakho         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef XYPOINT_HPP
#define XYPOINT_HPP

class XYPoint
{
public:
    XYPoint();
    XYPoint(unsigned x, unsigned y);
    XYPoint(const XYPoint&);
    ~XYPoint();

    void SetX(unsigned);
    void SetY(unsigned);
    unsigned GetX() const;
    unsigned GetY() const;

    bool operator>(const XYPoint&) const;
    bool operator<(const XYPoint&) const;
    bool operator==(const XYPoint&) const;
    bool operator!=(const XYPoint&) const;

    XYPoint& operator=(const XYPoint&);

private:
    unsigned _x;
    unsigned _y;
};

#endif /* XYPOINT_HPP */
