/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SpaceShip.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 07:34:54 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 20:22:18 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPACESHIP_HPP
#define SPACESHIP_HPP

#include "Bullet.hpp"
#include "SpaceObject.hpp"

class SpaceShip : public SpaceObject
{
public:
    SpaceShip();
    SpaceShip(const SpaceShip&);
    void MoveLeft();
    void MoveRight();
    void Heal(unsigned value = 10);
    Bullet *Shoot();

    SpaceShip& operator=(const SpaceShip&);
};

#endif /* SPACESHIP_HPP */
