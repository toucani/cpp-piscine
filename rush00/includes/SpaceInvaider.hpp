/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SpaceInvaider.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 08:01:07 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 20:31:52 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPACEINVAIDER_HPP
#define SPACEINVAIDER_HPP

#include "SpaceContainer.hpp"
#include "SpaceObject.hpp"
#include "Bullet.hpp"

class SpaceInvaider : public SpaceObject
{
public:
    SpaceInvaider();
    SpaceInvaider(const SpaceInvaider&);
    SpaceInvaider& operator=(const SpaceInvaider&);

    void Move();
    Bullet *Shoot();
    void SetPlayerPosition(const XYPoint&);
    void SetContainer(SpaceContainer&);

private:

    const XYPoint *p_cord;
    SpaceContainer *sp_c;
};

#endif /* SPACEINVAIDER_HPP */
