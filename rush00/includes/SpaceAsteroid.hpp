/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SpaceAsteroid.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 08:09:44 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 20:59:23 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPACEASTEROID_HPP
#define SPACEASTEROID_HPP

#include "SpaceObject.hpp"

class SpaceAsteroid : public SpaceObject
{
public:
    SpaceAsteroid();
    SpaceAsteroid(const SpaceAsteroid&);
    SpaceAsteroid& operator=(const SpaceAsteroid&);
    void Move();
};

#endif /* SPACEASTEROID_HPP */
