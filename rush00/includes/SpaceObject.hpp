/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SpaceObject.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 22:49:43 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 22:50:29 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPACEOBJECT_HPP
#define SPACEOBJECT_HPP

#define ABS(x) ((x) > 0 ? (x) : -(x))

#include "XYPoint.hpp"

class SpaceObject
{
public:
    SpaceObject(char view = '.', unsigned hp = 0, unsigned damage = 0, int col = 0);
    SpaceObject(const SpaceObject&);
    virtual ~SpaceObject();

    bool IsAlive() const;
    unsigned GetHp() const;

    char GetView() const;
    unsigned GetDamage() const;
    void TakeDamage(unsigned);
   // void show();
    virtual void Move();
    int     Get_color() const;
    const XYPoint& GetCoordinates() const;
    void SetCoordinates(const XYPoint&);
    bool Collide(const XYPoint&) const;
    bool Collide(const SpaceObject&) const;

    SpaceObject& operator=(const SpaceObject&);

protected:
    char _view;
    unsigned _hp;
    unsigned _damage;
    int     color;
    XYPoint _coordinates;
};

#endif /* SPACEOBJECT_HPP */
