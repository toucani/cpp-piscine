/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SpaceContainer.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 14:19:27 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 20:22:03 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPACECONTAINER_HPP
#define SPACECONTAINER_HPP

#include "SpaceObject.hpp"

class SpaceContainer
{
public:
    SpaceContainer();
    ~SpaceContainer();

    void Clear();
    void PushBack(SpaceObject*);
    void Delete(unsigned);
    void DeleteCurrent();
    int FindObject(const XYPoint&);
    unsigned Size() const;
    SpaceObject *Back();
    SpaceObject *GetCurrent();
    SpaceObject *operator[](unsigned);

private:
    SpaceContainer(const SpaceContainer&);
    SpaceContainer& operator=(const SpaceContainer&);

    unsigned _amount;
    unsigned _current;
    SpaceObject **_objects;
};

#endif /* CONTAINER_HPP */
