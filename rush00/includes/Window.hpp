/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Window.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: imarakho <imarakho@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 09:07:44 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 23:33:24 by imarakho         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <ncurses.h>
#include "SpaceObject.hpp"
#include "SpaceShip.hpp"

#define GetWindow Window::GetInstance()

class Window
{
public:
    static Window& GetInstance();
    ~Window();

    void Init();
    void Deinit();

    bool IsColored() const;
    void DrawBox();

    int GetChar() const;
    unsigned int GetMaxX() const;
    unsigned int GetMaxY() const;
    void Clear();
    void Clear(const XYPoint&);
    void Clear(unsigned x, unsigned y);

    void PutText(const std::string&);
    void PutObject(const SpaceObject&);
    void PutObject(const SpaceShip&);

private:
    Window();
    Window(const Window&);
    Window& operator=(const Window&);

    WINDOW* wnd;
    unsigned _maxX;
    unsigned _maxY;
};

#endif /* WINDOW_HPP */
