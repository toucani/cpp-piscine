/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bullet.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 20:23:45 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 21:13:22 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef Bullet_HPP
#define Bullet_HPP

#include "SpaceObject.hpp"

class Bullet : public SpaceObject
{
public:
    Bullet(unsigned int dam, XYPoint cord, bool pl = true);
    bool GoesUp() const;
    void Move();

private:
    Bullet();
    Bullet& operator=(const Bullet&);
    Bullet(const Bullet&);

    bool is_pl;
};

#endif /* Bullet_HPP */
