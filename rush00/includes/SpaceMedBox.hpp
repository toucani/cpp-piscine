/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SpaceMedBox.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 08:05:28 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 20:27:15 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPACEMEDBOX_HPP
#define SPACEMEDBOX_HPP

#include "SpaceObject.hpp"

class SpaceMedBox : public SpaceObject
{
public:
    SpaceMedBox();
    SpaceMedBox(const SpaceMedBox&);
    unsigned GetHealFactor() const;
    SpaceMedBox& operator=(const SpaceMedBox&);
};

#endif /* SPACEMEDBOX_HPP */
