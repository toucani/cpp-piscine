/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   XYPoint.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: imarakho <imarakho@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 22:42:06 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 15:59:01 by imarakho         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "XYPoint.hpp"

XYPoint::XYPoint() : _x(0), _y(0)
{
}

XYPoint::XYPoint(unsigned x, unsigned y) : _x(x), _y(y)
{
}

XYPoint::XYPoint(const XYPoint& other)
{
    *this = other;
}

XYPoint::~XYPoint()
{
}

void XYPoint::SetX(unsigned value)
{
    _x = value;
}

void XYPoint::SetY(unsigned value)
{
    _y = value;
}

unsigned XYPoint::GetX() const
{
    return _x;
}

unsigned XYPoint::GetY() const
{
    return _y;
}

bool XYPoint::operator>(const XYPoint& other) const
{
    return _x > other._x && _y > other._y;
}

bool XYPoint::operator<(const XYPoint& other) const
{
    return _x < other._x && _y < other._y;
}

bool XYPoint::operator==(const XYPoint& other) const
{
    return _x == other._x && _y == other._y;
}

bool XYPoint::operator!=(const XYPoint& other) const
{
    return !operator==(other);
}

XYPoint& XYPoint::operator=(const XYPoint& other)
{
    _x = other._x;
    _y = other._y;
    return *this;
}
