/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SpaceObject.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 22:49:36 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 22:56:14 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SpaceObject.hpp"

SpaceObject::SpaceObject(char view, unsigned hp, unsigned damage,int col)
    : _view(view), _hp(hp), _damage(damage), color(col)
{
}

SpaceObject::SpaceObject(const SpaceObject& other)
{
    *this = other;
}

SpaceObject::~SpaceObject()
{
}

bool SpaceObject::IsAlive() const
{
    return _hp > 0;
}

unsigned SpaceObject::GetHp() const
{
    return _hp;
}

char SpaceObject::GetView() const
{
    return _view;
}

unsigned SpaceObject::GetDamage() const
{
    return _damage;
}
/*
void Spaceobject::show() {
    attron(COLOR_PAIR(color));
    mvaddch(_coordinates.GetCoordinates().GetX(), _coordinates.GetCoordinates().GetX(), _view);
    attroff(COLORS);
}
*/
void SpaceObject::TakeDamage(unsigned value)
{
    if (value > _hp)
        _hp = 0;
    else
        _hp -= value;
}

void SpaceObject::Move()
{
}

const XYPoint& SpaceObject::GetCoordinates() const
{
    return _coordinates;
}

void SpaceObject::SetCoordinates(const XYPoint& coord)
{
    _coordinates = coord;
}

bool SpaceObject::Collide(const XYPoint& point) const
{
    return ABS(_coordinates.GetX() - point.GetX()) <= 1
        && ABS(_coordinates.GetY() - point.GetY()) <= 1;
}
bool SpaceObject::Collide(const SpaceObject& other) const
{
    return Collide(other.GetCoordinates());
}

int SpaceObject::Get_color() const
{
    return color;
}

SpaceObject& SpaceObject::operator=(const SpaceObject& other)
{
    _hp = other._hp;
    _damage = other._damage;
    _coordinates = other._coordinates;
    color = other.color;
    return *this;
}
