/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Window.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: imarakho <imarakho@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 14:54:17 by imarakho          #+#    #+#             */
/*   Updated: 2018/04/07 23:33:18 by imarakho         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sstream>
#include "Window.hpp"
#include <ncurses.h>
#include <cstdlib>

Window& Window::GetInstance()
{
    static Window wnd;
    return wnd;
}

Window::Window()
{
    wnd = NULL;
}

Window::~Window()
{
    Deinit();
}

void Window::Init()
{
    if (wnd)
        return;
    wnd = initscr();
    start_color();
    init_pair(0, COLOR_WHITE, COLOR_BLACK);
    init_pair(1, COLOR_YELLOW, COLOR_BLACK);
    init_pair(2, COLOR_RED, COLOR_BLACK);
    init_pair(3, COLOR_GREEN, COLOR_BLACK);
    init_pair(4, COLOR_BLUE, COLOR_BLACK);
    init_pair(5, COLOR_CYAN, COLOR_BLACK);
    cbreak();
    noecho();
    clear();
    curs_set(0);
    keypad(wnd, true);
    nodelay(wnd, true);
    getmaxyx(wnd, _maxY, _maxX);
    refresh();
}

void Window::Deinit()
{
    if (!wnd)
        return;
    endwin();
    wnd = NULL;
}

bool Window::IsColored() const
{
    return has_colors();
}

void Window::DrawBox()
{
    attron(A_BOLD);
    box(wnd, 0, 0);
    attroff(A_BOLD);
    refresh();
}

int Window::GetChar() const
{
    return wgetch(wnd);
}

void Window::Clear()
{
    clear();
}

unsigned int Window::GetMaxX() const
{
    return _maxX;
}

unsigned int Window::GetMaxY() const
{
    return _maxY;
}

void Window::Clear(const XYPoint& point)
{
    Clear(point.GetX(), point.GetY());
}

void Window::Clear(unsigned x, unsigned y)
{
    mvaddch(y, x, ' ');
}

void Window::PutText(const std::string& text)
{
    mvaddstr(_maxY / 2, _maxX / 2, text.c_str());
}

void Window::PutObject(const SpaceObject& obj)
{
    const unsigned objY = obj.GetCoordinates().GetY();
    const unsigned objX = obj.GetCoordinates().GetX();
    attron(COLOR_PAIR(obj.Get_color()));
    if(objX < 2 && obj.GetView() == '^')
        mvaddch(objY, objX + 1, obj.GetView());
    else if (objX > _maxX - 2 && obj.GetView() == '^')
        mvaddch(objY, objX - 1, obj.GetView());
    else
        mvaddch(objY, objX, obj.GetView());
    attroff(COLORS);
}

void Window::PutObject(const SpaceShip& ship)
{
    PutObject(static_cast<const SpaceObject&>(ship));
    std::stringstream ss;
    ss << "HP: ";
    if (ship.IsAlive())
        ss << ship.GetHp();
    else
        ss << "DEAD";
    mvaddstr(1, 1, ss.str().c_str());
}
