/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SpaceShip.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 07:36:22 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 22:45:11 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SpaceShip.hpp"

SpaceShip::SpaceShip() : SpaceObject('^', 25, 10, 5)
{
}

SpaceShip::SpaceShip(const SpaceShip& other) : SpaceObject(other)
{
}

void SpaceShip::MoveRight()
{
    _coordinates.SetX(_coordinates.GetX() + 1);
}

void SpaceShip::MoveLeft()
{
    if (_coordinates.GetX() > 1)
        _coordinates.SetX(_coordinates.GetX() - 1);
}

Bullet *SpaceShip::Shoot()
{
    XYPoint tmp = _coordinates;
    tmp.SetY(_coordinates.GetY() - 1);
    return new Bullet(_damage, tmp);
}

void SpaceShip::Heal(unsigned value)
{
    _hp += value;
}

SpaceShip& SpaceShip::operator=(const SpaceShip& other)
{
    SpaceObject::operator=(other);
    return *this;
}
