/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bullet.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 14:12:50 by imarakho          #+#    #+#             */
/*   Updated: 2018/04/07 21:13:44 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bullet.hpp"

Bullet::Bullet(unsigned int dam, XYPoint cord, bool pl) : SpaceObject('|', 1, dam)
{
    _coordinates = cord;
    is_pl = pl;
}

bool Bullet::GoesUp() const
{
    return is_pl;
}

void Bullet::Move()
{
    const unsigned result = _coordinates.GetY();
    if(is_pl)
        _coordinates.SetY(result - 1);
    else
        _coordinates.SetY(result + 1);
}