/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: imarakho <imarakho@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 17:48:38 by imarakho          #+#    #+#             */
/*   Updated: 2018/04/07 23:36:51 by imarakho         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <iostream>
#include "Window.hpp"
#include "Bullet.hpp"
#include "SpaceShip.hpp"
#include "SpaceMedBox.hpp"
#include "SpaceInvaider.hpp"
#include "SpaceAsteroid.hpp"
#include "SpaceContainer.hpp"

static SpaceShip CreatePlayer(Window& wind)
{
    SpaceShip player;
    //Putting the player into the middle of the 5th line from the end.
    player.SetCoordinates(XYPoint(wind.GetMaxX() / 2, wind.GetMaxY() - 5));
    wind.PutObject(player);
    return player;
}

static void AddStars(Window& wind, SpaceContainer& container)
{
    const unsigned amount = wind.GetMaxY() * wind.GetMaxX();

    //Adding stars
    for (unsigned i = 0; i < amount / 70; i++)
    {
        container.PushBack(new SpaceObject());
        container.Back()->SetCoordinates(XYPoint(rand() % wind.GetMaxX(), rand() % wind.GetMaxY()));
        wind.PutObject(*container.Back());
    }
}

static void AddAsteroids(Window& wind, SpaceContainer& container)
{
    const unsigned amount = wind.GetMaxY() * wind.GetMaxX();

    //Adding asteroids
    for (unsigned i = 0; i < amount / 60; i++)
    {
        container.PushBack(new SpaceAsteroid());
        container.Back()->SetCoordinates(XYPoint(rand() % (wind.GetMaxX() - 2) + 1, (rand() % (wind.GetMaxY() - 10)) + 1));
        wind.PutObject(*container.Back());
    }
}

static void AddMedBoxes(Window& wind, SpaceContainer& container)
{
    const unsigned amount = wind.GetMaxY() * wind.GetMaxX();

    //Adding healers
    for (unsigned i = 0; i < amount / 100; i++)
    {
        container.PushBack(new SpaceMedBox());
        container.Back()->SetCoordinates(XYPoint((rand() % (wind.GetMaxX() - 2)) + 1, (rand() % (wind.GetMaxY() - 10) - 1)));
        wind.PutObject(*container.Back());
    }
}

static void AddInvaiders(Window& wind, SpaceContainer& container, SpaceShip& player)
{
    //const unsigned amount = wind.GetMaxY() * wind.GetMaxX();

    //Adding invaiders
    for (unsigned i = 0; i < 10; i++)
    {
        SpaceInvaider * invaider = new SpaceInvaider();
        container.PushBack(invaider);
        invaider->SetContainer(container);
        invaider->SetPlayerPosition(player.GetCoordinates());
        invaider->SetCoordinates(XYPoint(rand() % wind.GetMaxX(), rand() % (wind.GetMaxY() - 15)));
        wind.PutObject(*container.Back());
    }
}

static void MoveBullet(Window& wind, SpaceShip& player, SpaceContainer& container, Bullet& bullet)
{
    wind.Clear(bullet.GetCoordinates());
    const XYPoint tmp(
            bullet.GetCoordinates().GetX(),
            bullet.GetCoordinates().GetY() + (bullet.GoesUp() ? - 1 : 1));
    const int i = container.FindObject(tmp);
    if (i >= 0)
    {
        wind.Clear(container[i]->GetCoordinates());
        container.Delete(i);
        container.DeleteCurrent();
    }
    else if (player.Collide(tmp))
    {
        player.TakeDamage(bullet.GetDamage());
    }
    else
    {
        bullet.Move();
        wind.PutObject(bullet);
    }
}

static void MoveObject(Window& wind, SpaceShip& player, SpaceContainer& container)
{
    wind.DrawBox();
    SpaceObject *curr = container.GetCurrent();
    if (!curr)
        return;

    switch(curr->GetView())
    {
        case '.' :
        case '+' :
            wind.PutObject(*curr);
        break;
        case '|' :
            MoveBullet(wind, player, container, *(static_cast<Bullet*>(curr)));
        break;
        case '*' :
        case '@' :
        default:
            wind.Clear(curr->GetCoordinates());
            curr->Move();
            wind.PutObject(*curr);
            if (player.Collide(*curr))
                player.TakeDamage(curr->GetDamage());
    }
}

static bool MovePlayer(Window& wind, SpaceShip& player, SpaceContainer& container)
{
    const int ch = wind.GetChar();
    switch(ch)
    {
        case KEY_LEFT:
        case 'a':
            if(player.GetCoordinates().GetX() > 1)
            {
                wind.Clear(player.GetCoordinates());
                player.MoveLeft();
            }
        break;
        case KEY_RIGHT:
        case 'd':
            if(player.GetCoordinates().GetX() < wind.GetMaxX() - 1)
            {
                wind.Clear(player.GetCoordinates());
                player.MoveRight();
            }
        break;
        case ' ':
        case 'f':
            container.PushBack(player.Shoot());
            wind.PutObject(*container.Back());
        break;
    }
    wind.PutObject(player);
    return ch == 'q';
}

static void RunMainLoop(Window& wind, SpaceShip& player, SpaceContainer& container)
{
    while(1)
    {
        MoveObject(wind, player, container);
        if (MovePlayer(wind, player, container))
            break;
        refresh();
        usleep(100);
        if (!player.IsAlive())
        {
            wind.Clear();
            wind.PutText("GAME OVER");
            refresh();
            usleep(5000000);//5sec
            break;
        }
    }
}

int main()
{
    srand(time(NULL));
    GetWindow.Init();
    if (!GetWindow.IsColored())
    {
        GetWindow.Deinit();
        std::cerr << "Error: No colors - no game." << std::endl;
    }
    else
    {
        Window& wind = GetWindow;
        wind.DrawBox();
        SpaceShip player = CreatePlayer(wind);
        SpaceContainer container;
        AddStars(wind, container);
        AddAsteroids(wind, container);
        AddMedBoxes(wind, container);
        AddInvaiders(wind, container, player);
        RunMainLoop(wind, player, container);
    }

    return 0;
}
