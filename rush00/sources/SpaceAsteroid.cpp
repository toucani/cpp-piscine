/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SpaceAsteroid.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: imarakho <imarakho@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 08:10:34 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 21:25:04 by imarakho         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SpaceAsteroid.hpp"

SpaceAsteroid::SpaceAsteroid() : SpaceObject('*', 5, 2, 1)
{
}

SpaceAsteroid::SpaceAsteroid(const SpaceAsteroid& other) : SpaceObject(other)
{
}

SpaceAsteroid& SpaceAsteroid::operator=(const SpaceAsteroid& other)
{
    SpaceObject::operator=(other);
    return *this;
}

void SpaceAsteroid::Move()
{
    _coordinates.SetY(_coordinates.GetY() + 1);
}