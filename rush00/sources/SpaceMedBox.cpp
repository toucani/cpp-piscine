/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SpaceMedBox.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 08:06:34 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 21:46:36 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SpaceMedBox.hpp"

SpaceMedBox::SpaceMedBox() : SpaceObject('+', 1, 10, 3)
{
}

SpaceMedBox::SpaceMedBox(const SpaceMedBox& other) : SpaceObject(other)
{
}

unsigned SpaceMedBox::GetHealFactor() const
{
    return SpaceObject::GetDamage();
}

SpaceMedBox& SpaceMedBox::operator=(const SpaceMedBox& other)
{
    SpaceObject::operator=(other);
    return *this;
}
