/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SpaceInvaider.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: imarakho <imarakho@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 08:03:07 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 21:24:53 by imarakho         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SpaceInvaider.hpp"

SpaceInvaider::SpaceInvaider() : SpaceObject('@', 10, 5, 2), p_cord(0)
{
}

SpaceInvaider::SpaceInvaider(const SpaceInvaider& other) : SpaceObject(other)
{
}

void SpaceInvaider::Move()
{
    if (!p_cord)
        return;

    if (p_cord->GetX() < _coordinates.GetX())
    {
        _coordinates.SetX(_coordinates.GetX() - 1);
    }
    else if (p_cord->GetX() > _coordinates.GetX())
    {
        _coordinates.SetX(_coordinates.GetX() + 1);
    }
    else if (sp_c)
    {
        sp_c->PushBack(new Bullet(_damage, _coordinates, false));
    }
}

Bullet *SpaceInvaider::Shoot()
{/*
    if (p_cord && p_cord->GetX() == _coordinates.GetX())
    {
        XYPoint tmp = _coordinates;
        tmp.SetY(_coordinates.GetY() + 1);
        return new Bullet(_damage, tmp, false);
    }*/
    return 0;
}

void SpaceInvaider::SetPlayerPosition(const XYPoint& position)
{
    p_cord = &position;
}

void SpaceInvaider::SetContainer(SpaceContainer& container)
{
    sp_c = &container;
}

SpaceInvaider& SpaceInvaider::operator=(const SpaceInvaider& other)
{
    SpaceObject::operator=(other);
    return *this;
}
