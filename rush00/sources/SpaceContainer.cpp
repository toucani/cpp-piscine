/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SpaceContainer.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 14:24:05 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/07 22:41:10 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SpaceContainer.hpp"

SpaceContainer::SpaceContainer() : _amount(0), _current(0), _objects(0)
{
}

SpaceContainer::~SpaceContainer()
{
    Clear();
}

void SpaceContainer::Clear()
{
    for (unsigned ct = 0; ct < _amount; ct++)
        delete _objects[ct];
    delete[] _objects;
    _objects = 0;
    _amount = 0;
}

void SpaceContainer::PushBack(SpaceObject* obj)
{
    SpaceObject **newArray = new SpaceObject*[_amount > 0 ? _amount * 2 : 2];

    for (unsigned ct = 0; ct < _amount; ct++)
        newArray[ct] = _objects[ct];
    delete[] _objects;

    _objects = newArray;
    _objects[_amount++] = obj;
}

void SpaceContainer::Delete(unsigned position)
{
    if (operator[](position))
    {
        delete _objects[position];
        _objects[position] = 0;
    }
}

void SpaceContainer::DeleteCurrent()
{
    Delete(_current > 0 ? _current : _amount - 1);
}

int SpaceContainer::FindObject(const XYPoint& position)
{
    for (unsigned i = 0; i < _amount; i++)
    {
        if (_objects[i] && _objects[i]->GetView() != '.' //Skipping stars.
            && _objects[i]->GetCoordinates() == position)
            return i;
    }
    return -1;
}

SpaceObject *SpaceContainer::Back()
{
    return _amount > 0 ? _objects[_amount - 1] : 0;
}

unsigned SpaceContainer::Size() const
{
    return _amount;
}

SpaceObject *SpaceContainer::GetCurrent()
{
    SpaceObject * result = operator[](_current++);
    _current = _current >= _amount ? 0 : _current;
    if(!result)
        return GetCurrent();
    return result;
}

SpaceObject *SpaceContainer::operator[](unsigned id)
{
    return ((id >= _amount) ? 0 : _objects[id]);
}
