/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 21:44:28 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/06 22:21:41 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Squad.hpp"

Squad::Squad() : _amount(0), _marines()
{
}

Squad::Squad(const Squad& other)
{
    *this = other;
}

Squad::~Squad()
{
    for (int ct = 0; ct < _amount; ct++)
        delete _marines[ct];
    delete[] _marines;
}

int Squad::getCount() const
{
    return _amount;
}

ISpaceMarine* Squad::getUnit(int n) const
{
    if (n < 0 || n >= _amount || !_marines)
        return 0;
    return _marines[n];
}

int Squad::push(ISpaceMarine* unit)
{
    ISpaceMarine **newMarines = new ISpaceMarine*[_amount + 1];

    for (int ct = 0; ct < _amount; ct++)
        newMarines[ct] = _marines[ct];
    delete[] _marines;

    _marines = newMarines;
    _marines[_amount] = unit;

    return ++_amount;
}

Squad& Squad::operator=(const Squad& other)
{
    for (int ct = 0; ct < _amount; ct++)
        delete _marines[ct];
    delete[] _marines;

    _amount = other._amount;
    _marines = new ISpaceMarine*[other._amount];
    for (int ct = 0; ct < _amount; ct++)
        _marines[ct] = other._marines[ct]->clone();
    return *this;
}
