/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 21:40:52 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/06 22:18:48 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SQUAD_HPP
#define SQUAD_HPP

#include "ISquad.hpp"

class Squad : public ISquad
{
public:
    Squad();
    Squad(const Squad&);
    ~Squad();

    int getCount() const;
    ISpaceMarine* getUnit(int) const;
    int push(ISpaceMarine*);

    Squad& operator=(const Squad&);
private:
    int _amount;
    ISpaceMarine** _marines;
};

#endif /* SQUAD_HPP */
