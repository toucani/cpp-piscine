/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 09:56:24 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/06 12:13:52 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AWEAPON_HPP
#define AWEAPON_HPP

#include <string>

class AWeapon
{
    AWeapon();
public:
    AWeapon(const std::string& name, int apcost, int damage);
    AWeapon(const AWeapon&);
    virtual ~AWeapon();

    std::string getName() const;
    int getAPCost() const;
    int getDamage() const;
    virtual void attack() const = 0;

    AWeapon& operator=(const AWeapon&);

private:
    std::string _name;
    int _apCost;
    int _damage;
};

#endif /* AWEAPON_HPP */
