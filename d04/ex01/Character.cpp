/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 11:32:18 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/06 12:13:55 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"
#include <iostream>

Character::Character(const std::string& name)
    : _name(name), _ap(40), _weapon(nullptr)
{
}

Character::Character(const Character& other)
{
    *this = other;
}

Character::~Character()
{
}

void Character::recoverAP()
{
    _ap = std::min(_ap + 10, 40);
}

void Character::equip(AWeapon* weapon)
{
    _weapon = weapon;
}

void Character::attack(Enemy* enemy)
{
    if (!enemy || !_weapon || _ap < _weapon->getAPCost())
        return;
    std::cout << _name << " attacks " << enemy->getType() << " with a "
        << _weapon->getName() << "." << std::endl;
    _weapon->attack();
    enemy->takeDamage(_weapon->getDamage());
    _ap = std::max(_ap - _weapon->getAPCost(), 0);
    if (enemy->getHP() == 0)
        delete enemy;
}

const std::string& Character::getName() const
{
    return _name;
}

int Character::getAp() const
{
    return _ap;
}

AWeapon *Character::getWeapon() const
{
    return _weapon;
}

Character& Character::operator=(const Character& other)
{
    _name = other._name;
    _ap = other._ap;
    _weapon = other._weapon;
    return *this;
}

std::ostream& operator<<(std::ostream& os, const Character& ch)
{
    os << ch.getName() << " has " << ch.getAp() << " ap and ";
    if (ch.getWeapon())
        os << "wields a " << ch.getWeapon()->getName() << ".";
    else
        os << "is unarmed.";
    os << std::endl;
    return os;
}
