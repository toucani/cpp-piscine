/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 09:59:59 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/06 12:13:50 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AWeapon.hpp"

AWeapon::AWeapon(const std::string& name, int apcost, int damage)
    : _name(name), _apCost(apcost), _damage(damage)
{
}

AWeapon::AWeapon(const AWeapon& other)
{
    *this = other;
}

AWeapon::~AWeapon()
{
}

std::string AWeapon::getName() const
{
    return _name;
}

int AWeapon::getAPCost() const
{
    return _apCost;
}

int AWeapon::getDamage() const
{
    return _damage;
}

AWeapon& AWeapon::operator=(const AWeapon& other)
{
    _name = other._name;
    _damage = other._damage;
    _apCost = other._apCost;
    return *this;
}
