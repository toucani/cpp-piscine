/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 11:06:10 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/06 12:14:06 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Enemy.hpp"

Enemy::Enemy(int hp, const std::string& type)
    : _hp(hp), _type(type)
{
}

Enemy::Enemy(const Enemy& other)
{
    *this = other;
}

Enemy::~Enemy()
{
}

const std::string& Enemy::getType() const
{
    return _type;
}

int Enemy::getHP() const
{
    return _hp;
}

void Enemy::takeDamage(int amount)
{
    if (_hp <= 0)
        return;
    _hp = std::max(_hp - amount, 0);
}

Enemy& Enemy::operator=(const Enemy& other)
{
    _hp = other._hp;
    _type = other._type;
    return *this;
}
