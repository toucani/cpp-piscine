/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 10:16:47 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/06 12:14:08 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENEMY_HPP
#define ENEMY_HPP

#include <string>

class Enemy
{
    Enemy();
public:
    Enemy(int hp, const std::string& type);
    Enemy(const Enemy&);
    virtual ~Enemy();

    const std::string& getType() const;
    int getHP() const;
    virtual void takeDamage(int);

    Enemy& operator=(const Enemy&);
private:
    int _hp;
    std::string _type;
};

#endif /* ENEMY_HPP */
