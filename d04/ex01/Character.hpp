/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 11:29:31 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/06 12:14:02 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHARACTER_HPP
#define CHARACTER_HPP

#include <string>
#include "AWeapon.hpp"
#include "Enemy.hpp"
#include <ostream>

class Character
{
    Character();
public:
    Character(const std::string& name);
    Character(const Character&);
    ~Character();

    void recoverAP();
    void equip(AWeapon*);
    void attack(Enemy*);

    const std::string& getName() const;
    int getAp() const;
    AWeapon *getWeapon() const;

    Character& operator=(const Character&);

private:
    std::string _name;
    int _ap;
    AWeapon *_weapon;

};
std::ostream& operator<<(std::ostream&, const Character&);
#endif /* CHARACTER_HPP */
