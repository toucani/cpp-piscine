/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 12:13:43 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/06 12:13:44 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Victim.hpp"

Victim::Victim(const std::string& name)
    : _name(name)
{
    std::cout << "Some random victim called " << _name << " has been popped!" << std::endl;
}

Victim::~Victim()
{
    std::cout << "Victim " << _name << " just died for no apparent reason!" << std::endl;
}

const std::string& Victim::GetName() const
{
    return _name;
}

void Victim::getPolymorphed() const
{
    std::cout << _name << " has been turned into a cute little sheep!" << std::endl;
}

Victim& Victim::operator=(const Victim& other)
{
    _name = other._name;
    return *this;
}


std::ostream& operator<<(std::ostream& os, const Victim& v)
{
    os << "I'm " << v.GetName() << " and I like otters!" << std::endl;
    return os;
}