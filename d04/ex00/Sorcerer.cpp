/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 12:13:37 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/06 12:13:38 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Sorcerer.hpp"
#include <iostream>

Sorcerer::Sorcerer(const std::string& name, const std::string& title)
    : _name(name), _title(title)
{
    std::cout << _name << ", " << _title << " has been born!" << std::endl;
}

Sorcerer::~Sorcerer()
{
    std::cout << _name << ", " << _title << " is dead. Consequences will never be the same!" << std::endl;
}

const std::string& Sorcerer::GetName() const
{
    return _name;
}

const std::string& Sorcerer::GetTitle() const
{
    return _title;
}

void Sorcerer::polymorph(const Victim& v) const
{
    v.getPolymorphed();
}

Sorcerer& Sorcerer::operator=(const Sorcerer& other)
{
    _name = other._name;
    _title = other._title;
    return *this;
}

std::ostream& operator<<(std::ostream& os, const Sorcerer& s)
{
    os << "I am " << s.GetName() << ", " << s.GetTitle() << ", and I like ponies!" << std::endl;
    return os;
}