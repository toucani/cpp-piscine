/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 12:13:46 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/06 12:13:47 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VICTIM_HPP
#define VICTIM_HPP

#include <string>
#include <iostream>

class Victim
{
    Victim();
public:
    Victim(const std::string& name);
    Victim(const Victim&);
    ~Victim();

    const std::string& GetName() const;

    virtual void getPolymorphed() const;

    Victim& operator=(const Victim&);

protected:
    std::string _name;
};

std::ostream& operator<<(std::ostream&, const Victim&);
#endif /* VICTIM_HPP */
