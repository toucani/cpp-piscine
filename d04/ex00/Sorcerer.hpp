/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 12:13:41 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/06 12:13:42 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SORCERER_HPP
#define SORCERER_HPP

#include <string>
#include <ostream>
#include "Victim.hpp"

class Sorcerer
{
    Sorcerer();
public:
    Sorcerer(const std::string& name, const std::string& title);
    Sorcerer(const Sorcerer&);
    ~Sorcerer();

    const std::string& GetName() const;
    const std::string& GetTitle() const;

    void polymorph(const Victim&) const;

    Sorcerer& operator=(const Sorcerer&);

private:
    std::string _name, _title;
};

std::ostream& operator<<(std::ostream&, const Sorcerer&);

#endif /* SORCERER_HPP */
