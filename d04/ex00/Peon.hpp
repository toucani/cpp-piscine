/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 12:13:31 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/06 12:13:32 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PEON_HPP
#define PEON_HPP

#include "Victim.hpp"

class Peon : public Victim
{
    Peon();
public:
    Peon(const std::string& name);
    Peon(const Peon&);
    ~Peon();

    void getPolymorphed() const;

    Peon& operator=(const Peon&);
};

#endif /* PEON_HPP */
