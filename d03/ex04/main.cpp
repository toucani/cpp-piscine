/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 09:21:37 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 19:33:49 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SuperTrap.hpp"
#include <cstdlib>
#include <iostream>

int main()
{
    srand(time(nullptr));
    FragTrap target("Target trap");
    std::cout << "=========Super trap logs==========" << std::endl;
    SuperTrap super("BigSuperTrap");
    super.rangedAttack("dummy target");
    super.meleeAttack("dummy target");
    super.takeDamage(34);
    super.beRepaired(100500);
    super.takeDamage(50500);
    super.beRepaired(100500);
    super.ninjaShoebox(target);
    super.vaulthunter_dot_exe("Dummy zombie");

    return 0;
}
