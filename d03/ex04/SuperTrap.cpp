/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 16:59:16 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 18:37:50 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SuperTrap.hpp"
#include <iostream>

SuperTrap::SuperTrap() : FragTrap(), NinjaTrap()
{
    std::cout << "SuperTrap constructor\n";
    _type = "SuperTrap";
    Startup();
    Reset();
}

SuperTrap::SuperTrap(const std::string& name) : FragTrap(name), NinjaTrap(name)
{
    std::cout << "SuperTrap constructor\n";
    _name = name;
    _type = "SuperTrap";
    Startup();
    Reset();
}

SuperTrap::SuperTrap(const SuperTrap& other) : FragTrap(other), NinjaTrap(other)
{
    std::cout << "SuperTrap constructor\n";
}

SuperTrap::~SuperTrap()
{
    std::cout << "SuperTrap destructor\n";
}

void SuperTrap::rangedAttack(const std::string& target) const
{
    FragTrap::rangedAttack(target);
}

void SuperTrap::meleeAttack(const std::string& target) const
{
    NinjaTrap::meleeAttack(target);
}

SuperTrap& SuperTrap::operator=(const SuperTrap& other)
{
    NinjaTrap::operator=(other);
    return *this;
}

void SuperTrap::Startup()
{
    std::cout << "Loading super command sequence from: " << this << std::endl;
}

void SuperTrap::Reset()
{
    std::cout << "Resetting SuperTrap stats..." << std::endl;
    _max_hp = 100;
    _max_ep = 120;
    _hp = _max_hp;
    _ep = _max_ep;
    _level = 1;
    _damage_melee = 60;
    _damage_ranged = 20;
    _armor_red = 5;
}