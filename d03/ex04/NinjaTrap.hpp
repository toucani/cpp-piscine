/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 15:18:08 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 18:10:28 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NINJATRAP_HPP
#define NINJATRAP_HPP

#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

class NinjaTrap : public virtual ClapTrap
{
public:
    NinjaTrap();
    NinjaTrap(const std::string& name);
    NinjaTrap(const NinjaTrap&);
    virtual ~NinjaTrap();

    void ninjaShoebox(const ClapTrap& target);
    void ninjaShoebox(const NinjaTrap& target);
    void ninjaShoebox(const FragTrap& target);
    void ninjaShoebox(const ScavTrap& target);

    NinjaTrap& operator=(const NinjaTrap&);

private:
    void Startup();
    void Reset();
};

#endif /* NINJATRAP_HPP */
