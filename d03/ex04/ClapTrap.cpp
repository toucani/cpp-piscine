/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 13:47:22 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 18:35:26 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"
#include <iostream>

ClapTrap::ClapTrap() : _type("ClapTrap"), _name("NoName")
{
    std::cout << "Basic ClapTrap constructor " << this << std::endl;
    Reset();
}
ClapTrap::ClapTrap(const std::string& type, const std::string& name ) : _type(type), _name(name)
{
    std::cout << "Basic ClapTrap constructor for " << _type << " " << _name << " at " << this << std::endl;
    Reset();
}

ClapTrap::ClapTrap(const ClapTrap& other) : _type(other._type), _name(other._name)
{
    std::cout << "Basic ClapTrap constructor " << this << std::endl;
    *this = other;
}

ClapTrap::~ClapTrap()
{
    std::cout << "Basic ClapTrap destructor." << std::endl;
}

void ClapTrap::rangedAttack(const std::string& target) const
{
    std::cout << _type << " " << _name << " attacks " << target
        << " at range, causing " << _damage_ranged << " damage points. " << std::endl;
}

void ClapTrap::meleeAttack(const std::string& target) const
{
    std::cout << _type << " " << _name << " attacks " << target
        << " melee, causing " << _damage_melee << " damage points. " << std::endl;
}

void ClapTrap::takeDamage(unsigned int amount)
{
    amount -= _armor_red;
    _hp = (amount >= _hp) ? 0 : (_hp - amount);

    std::cout << _type << " " << _name << " takes " << amount
        << " damage, reducing hp to " << _hp << "." << std::endl;
    if (_hp == 0)
        std::cout << _type << " " << _name << " has been knocked out!" << std::endl;
}

void ClapTrap::beRepaired(unsigned int amount)
{
    const bool alive = _hp == 0;
    _hp = (amount >= _max_hp - _hp) ? _max_hp : (_hp + amount);

    std::cout << _type << " " << _name << " repairs " << amount
        << " of damage, restoring hp to " << _hp << "." << std::endl;
    if (alive)
        std::cout << _type << " " << _name << " is back and operational!" << std::endl;
}

std::string ClapTrap::GetString() const
{
    return _type + " " + _name;
}

ClapTrap& ClapTrap::operator=(const ClapTrap& other)
{
    std::cout << _type << " " << _name << "copies itself from "
        << other._type << " " << other._name << std::endl;
    _hp = other._hp;
    _max_hp = other._max_hp;
    _ep = other._ep;
    _max_ep = other._max_ep;
    _level = other._level;
    _damage_melee = other._damage_melee;
    _damage_ranged = other._damage_ranged;
    _armor_red = other._armor_red;
    return *this;
}

void ClapTrap::Reset()
{
    std::cout << "Basic ClapTrap cleanup for "
        << _type << " " << _name << std::endl;
    _max_hp = 100;
    _max_ep = 100;
    _hp = _max_hp;
    _ep = _max_ep;
    _level = 1;
    _damage_melee = 10;
    _damage_ranged = 5;
    _armor_red = 0;
}
