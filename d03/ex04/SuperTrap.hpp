/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 16:59:13 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 18:14:39 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SUPERTRAP_HPP
#define SUPERTRAP_HPP

#include "FragTrap.hpp"
#include "NinjaTrap.hpp"

class SuperTrap : public FragTrap, public NinjaTrap
{
public:
    SuperTrap();
    SuperTrap(const std::string& name);
    SuperTrap(const SuperTrap&);
    ~SuperTrap();

    void rangedAttack(const std::string& target) const;
    void meleeAttack(const std::string& target) const;
    SuperTrap& operator=(const SuperTrap&);

private:
    void Startup();
    void Reset();
};

#endif /* SUPERTRAP_HPP */
