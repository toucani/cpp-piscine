/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 09:21:09 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 13:52:44 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include <cstdlib>
#include <iostream>

int main()
{
    srand(time(nullptr));
    FragTrap frag("main guy");
    FragTrap secondary(frag);
    secondary = FragTrap("the other guy");

    std::cout << "=========TEST==========" << std::endl;
    frag.rangedAttack("dummy target");
    frag.rangedAttack("dummy target");
    frag.meleeAttack("dummy target");
    frag.takeDamage(34);
    frag.beRepaired(100500);
    frag.takeDamage(50500);
    frag.beRepaired(100500);
    frag.vaulthunter_dot_exe("random zombie");
    frag.vaulthunter_dot_exe("random zombie");
    frag.vaulthunter_dot_exe("random zombie");
    frag.vaulthunter_dot_exe("random zombie");
    frag.vaulthunter_dot_exe("random zombie");

    return 0;
}
