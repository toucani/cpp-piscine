/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 09:21:37 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 13:52:57 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include <cstdlib>
#include <iostream>

int main()
{
    srand(time(nullptr));
    FragTrap frag("main guy");

    std::cout << "=========TEST FRAG==========" << std::endl;
    frag.rangedAttack("dummy target");
    frag.rangedAttack("dummy target");
    frag.meleeAttack("dummy target");
    frag.takeDamage(34);
    frag.beRepaired(100500);
    frag.takeDamage(50500);
    frag.beRepaired(100500);
    frag.vaulthunter_dot_exe("random zombie");
    frag.vaulthunter_dot_exe("random zombie");
    frag.vaulthunter_dot_exe("random zombie");
    frag.vaulthunter_dot_exe("random zombie");
    frag.vaulthunter_dot_exe("random zombie");

    std::cout << "=========TEST SCAV==========" << std::endl;
    ScavTrap scav("the other guy");
    scav.rangedAttack("dummy target");
    scav.rangedAttack("dummy target");
    scav.meleeAttack("dummy target");
    scav.takeDamage(34);
    scav.beRepaired(100500);
    scav.takeDamage(50500);
    scav.beRepaired(100500);
    scav.challengeNewcomer("random zombie");
    scav.challengeNewcomer("random zombie");
    scav.challengeNewcomer("random zombie");
    scav.challengeNewcomer("random zombie");
    scav.challengeNewcomer("random zombie");

    return 0;
}
