/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 09:21:40 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 13:53:01 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"
#include <iostream>

ScavTrap::ScavTrap()
{
    Startup();
    Reset();
}

ScavTrap::ScavTrap(const std::string& name)
{
    Startup();
    Reset(name);
}

ScavTrap::ScavTrap(const ScavTrap& other)
{
    Startup();
    *this = other;
}

ScavTrap::~ScavTrap()
{
    std::cout << "Shutting down " << _name << "..." << std::endl;
}

void ScavTrap::rangedAttack(const std::string& target) const
{
    std::cout << "ScavTrap " << _name << " attacks " << target
        << " at range, causing " << _damage_ranged << " damage points. " << std::endl;
}

void ScavTrap::meleeAttack(const std::string& target) const
{
    std::cout << "ScavTrap " << _name << " attacks " << target
        << " melee, causing " << _damage_melee << " damage points. " << std::endl;
}

void ScavTrap::takeDamage(unsigned int amount)
{
    amount -= _armor_red;
    _hp = (amount >= _hp) ? 0 : (_hp - amount);

    std::cout << "ScavTrap " << _name << " takes " << amount
        << " damage, reducing hp to " << _hp << "." << std::endl;
    if (_hp == 0)
        std::cout << "ScavTrap " << _name << " knocked out!" << std::endl;
}

void ScavTrap::beRepaired(unsigned int amount)
{
    const bool alive = _hp == 0;
    _hp = (amount >= _max_hp - _hp) ? _max_hp : (_hp + amount);

    std::cout << "ScavTrap " << _name << " repairs " << amount
        << " of damage, restoring hp to " << _hp << "." << std::endl;
    if (alive)
        std::cout << "ScavTrap " << _name << " is back and operational!" << std::endl;
}

void ScavTrap::challengeNewcomer(const std::string& target)
{
    switch (rand() % 5)
    {
        case 0:
            rangedAttack(target);
        break;
        case 1:
            meleeAttack(target);
        break;
        case 2:
            std::cout << "ScavTrap " << _name << " challanges " << target
                << " with a rubick's cube." << std::endl;
        break;
        case 3:
            std::cout << "Call 111-99-3342(ask ScavTrap) to place your ads here!!! " << std::endl;
        break;
        default:
            std::cout << "ScavTrap " << _name
                << " updates Windows Vista paying no attention to "<< target
                << "." << std::endl;
        break;
    }
}

ScavTrap& ScavTrap::operator=(const ScavTrap& other)
{
    _name = other._name;
    _hp = other._hp;
    _max_hp = other._max_hp;
    _ep = other._ep;
    _max_ep = other._max_ep;
    _level = other._level;
    _damage_melee = other._damage_melee;
    _damage_ranged = other._damage_ranged;
    _armor_red = other._armor_red;
    return *this;
}

void ScavTrap::Startup()
{
    std::cout << "Starting booting sequence from: " << this
        << "\n\tChecking interpreters..."
        << "\n\t寻找蛋糕..."
        << "\n\tケーキを探している..."
        << "\n\tకేక్ కోసం వెతుకుతున్నాను..."
        << "\n\tSerĉante la kukon...\n"
        << "Booting sequence complete." << std::endl;
}

void ScavTrap::Reset(const std::string& name)
{
    std::cout << "Clearing up memoty banks...\nChanging identity from "
        << (_name.empty() ? "(null)" : _name)
        << " to " << name << "." << std::endl;
    _name = name;
    _hp = 100;
    _max_hp = 100;
    _ep = 50;
    _max_ep = 50;
    _level = 1;
    _damage_melee = 20;
    _damage_ranged = 15;
    _armor_red = 3;
}
