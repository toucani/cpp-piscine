/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 09:21:28 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 13:53:04 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAVTRAP_HPP
#define SCAVTRAP_HPP

#include <string>

class ScavTrap
{
public:
    ScavTrap();
    ScavTrap(const std::string& name);
    ScavTrap(const ScavTrap&);
    ~ScavTrap();

    void rangedAttack(const std::string& target) const;
    void meleeAttack(const std::string& target) const;
    void takeDamage(unsigned int amount);
    void beRepaired(unsigned int amount);
    void challengeNewcomer(const std::string& target);

    ScavTrap& operator=(const ScavTrap&);

private:
    void Startup();
    void Reset(const std::string& name = "");
    unsigned _hp;
    unsigned _max_hp;
    unsigned _ep;
    unsigned _max_ep;
    unsigned _level;
    std::string _name;
    unsigned _damage_melee;
    unsigned _damage_ranged;
    unsigned _armor_red;
};

#endif /* SCAVTRAP_HPP */
