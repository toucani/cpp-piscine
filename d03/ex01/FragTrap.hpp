/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 09:21:34 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 13:52:53 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAGTRAP_HPP
#define FRAGTRAP_HPP

#include <string>

class FragTrap
{
public:
    FragTrap();
    FragTrap(const std::string& name);
    FragTrap(const FragTrap&);
    ~FragTrap();

    void rangedAttack(const std::string& target) const;
    void meleeAttack(const std::string& target) const;
    void takeDamage(unsigned int amount);
    void beRepaired(unsigned int amount);
    void vaulthunter_dot_exe(const std::string& target);

    FragTrap& operator=(const FragTrap&);

private:
    void Startup();
    void Reset(const std::string& name = "");
    unsigned _hp;
    unsigned _max_hp;
    unsigned _ep;
    unsigned _max_ep;
    unsigned _level;
    std::string _name;
    unsigned _damage_melee;
    unsigned _damage_ranged;
    unsigned _armor_red;
};

#endif /* FRAGTRAP_HPP */
