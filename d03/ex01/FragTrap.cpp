/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 09:21:31 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 13:52:50 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include <iostream>

FragTrap::FragTrap()
{
    Startup();
    Reset();
}

FragTrap::FragTrap(const std::string& name)
{
    Startup();
    Reset(name);
}

FragTrap::FragTrap(const FragTrap& other)
{
    Startup();
    *this = other;
}

FragTrap::~FragTrap()
{
    std::cout << "Shutting down " << _name << "..." << std::endl;
}

void FragTrap::rangedAttack(const std::string& target) const
{
    std::cout << "FR4G-TP " << _name << " attacks " << target
        << " at range, causing " << _damage_ranged << " damage points. " << std::endl;
}

void FragTrap::meleeAttack(const std::string& target) const
{
    std::cout << "FR4G-TP " << _name << " attacks " << target
        << " melee, causing " << _damage_melee << " damage points. " << std::endl;
}

void FragTrap::takeDamage(unsigned int amount)
{
    amount -= _armor_red;
    _hp = (amount >= _hp) ? 0 : (_hp - amount);

    std::cout << "FR4G-TP " << _name << " takes " << amount
        << " damage, reducing hp to " << _hp << "." << std::endl;
    if (_hp == 0)
        std::cout << "FR4G-TP " << _name << " knocked out!" << std::endl;
}

void FragTrap::beRepaired(unsigned int amount)
{
    const bool alive = _hp == 0;
    _hp = (amount >= _max_hp - _hp) ? _max_hp : (_hp + amount);

    std::cout << "FR4G-TP " << _name << " repairs " << amount
        << " of damage, restoring hp to " << _hp << "." << std::endl;
    if (alive)
        std::cout << "FR4G-TP " << _name << " is back and operational!" << std::endl;
}

void FragTrap::vaulthunter_dot_exe(const std::string& target)
{
    if (_ep < 25)
    {
        std::cout << "FR4G-TP " << _name << " tries to attack " << target
            << " with a vaulthunter_dot_exe, but suffers energy loss."
            << std::endl;
        return;
    }
    _ep -= 25;
    switch (rand() % 5)
    {
        case 0:
            rangedAttack(target);
        break;
        case 1:
            meleeAttack(target);
        break;
        case 2:
            std::cout << "FR4G-TP " << _name << " attacks " << target
                << " with a bunch of stupid jokes, causing headache."
                << std::endl;
        break;
        case 3:
            std::cout << "FR4G-TP " << _name
                << " is depressed and refuses to attack " << target
                << " depressing it as well and causing 15 points of moral damage."
                << std::endl;
        break;
        case 4:
            std::cout << "FR4G-TP " << _name << " attacks " << target
                << " at range, using Meltwodn and Spectre, causing immediate Blue Screen of Death."
                << std::endl;
        break;
        default:
            std::cout << "FR4G-TP " << _name
                << " updates Windows Vista paying no attention to "<< target
                << "." << std::endl;
        break;
    }
}

FragTrap& FragTrap::operator=(const FragTrap& other)
{
    _name = other._name;
    _hp = other._hp;
    _max_hp = other._max_hp;
    _ep = other._ep;
    _max_ep = other._max_ep;
    _level = other._level;
    _damage_melee = other._damage_melee;
    _damage_ranged = other._damage_ranged;
    _armor_red = other._armor_red;
    return *this;
}

void FragTrap::Startup()
{
    std::cout << "Starting booting sequence from: "
        << static_cast<const void*>(this)
        << "\n\tDirective one: Protect humanity!"
        << "\n\tDirective two: Obey Jack at all costs."
        << "\n\tDirective three: Dance!\n"
        << "Booting sequence complete." << std::endl;
}

void FragTrap::Reset(const std::string& name)
{
    std::cout << "Clearing up memoty banks...\nChanging identity from "
        << (_name.empty() ? "(null)" : _name)
        << " to " << name << "." << std::endl;
    _name = name;
    _hp = 100;
    _max_hp = 100;
    _ep = 100;
    _max_ep = 100;
    _level = 1;
    _damage_melee = 30;
    _damage_ranged = 20;
    _armor_red = 5;
}
