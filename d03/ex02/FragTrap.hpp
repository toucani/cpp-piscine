/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 09:21:34 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 14:23:22 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAGTRAP_HPP
#define FRAGTRAP_HPP

#include "ClapTrap.hpp"

class FragTrap : public ClapTrap
{
public:
    FragTrap();
    FragTrap(const std::string& name);
    FragTrap(const FragTrap&);
    ~FragTrap();

    void vaulthunter_dot_exe(const std::string& target);

    FragTrap& operator=(const FragTrap&);

private:
    void Startup();
    void Reset();
};

#endif /* FRAGTRAP_HPP */
