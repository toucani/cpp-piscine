/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 09:21:40 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 15:28:59 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"
#include <iostream>

ScavTrap::ScavTrap() : ClapTrap("ScavTrap")
{
    std::cout << "ScavTrap constructor\n";
    Startup();
    Reset();
}

ScavTrap::ScavTrap(const std::string& name) : ClapTrap("ScavTrap", name)
{
    std::cout << "ScavTrap constructor\n";
    Startup();
    Reset();
}

ScavTrap::ScavTrap(const ScavTrap& other) : ClapTrap(other)
{
    std::cout << "ScavTrap constructor\n";
}

ScavTrap::~ScavTrap()
{
    std::cout << "ScavTrap destructor\n";
}

void ScavTrap::challengeNewcomer(const std::string& target)
{
    switch (rand() % 5)
    {
        case 0:
            rangedAttack(target);
        break;
        case 1:
            meleeAttack(target);
        break;
        case 2:
            std::cout << "ScavTrap " << _name << " challanges " << target
                << " with a rubick's cube." << std::endl;
        break;
        case 3:
            std::cout << "Call 111-99-3342(ask ScavTrap) to place your ads here!!! " << std::endl;
        break;
        default:
            std::cout << "ScavTrap " << _name
                << " updates Windows Vista paying no attention to "<< target
                << "." << std::endl;
        break;
    }
}

ScavTrap& ScavTrap::operator=(const ScavTrap& other)
{
    ClapTrap::operator=(other);
    return *this;
}

void ScavTrap::Startup()
{
    std::cout << "Starting booting sequence from: " << this
        << "\n\tChecking interpreters..."
        << "\n\t寻找蛋糕..."
        << "\n\tケーキを探している..."
        << "\n\tకేక్ కోసం వెతుకుతున్నాను..."
        << "\n\tSerĉante la kukon...\n"
        << "Booting sequence complete." << std::endl;
}

void ScavTrap::Reset()
{
    std::cout << "Resetting ScavTrap stats..." << std::endl;
    _max_hp = 100;
    _max_ep = 50;
    _hp = _max_hp;
    _ep = _max_ep;
    _level = 1;
    _damage_melee = 20;
    _damage_ranged = 15;
    _armor_red = 3;
}