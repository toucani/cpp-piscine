/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 09:21:31 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 19:29:58 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include <iostream>

FragTrap::FragTrap() : ClapTrap("FragTrap")
{
    std::cout << "FragTrap constructor\n";
    Startup();
    Reset();
}

FragTrap::FragTrap(const std::string& name) : ClapTrap("FragTrap", name)
{
    std::cout << "FragTrap constructor\n";
    Startup();
    Reset();
}

FragTrap::FragTrap(const FragTrap& other) : ClapTrap(other)
{
    std::cout << "FragTrap constructor\n";
}

FragTrap::~FragTrap()
{
    std::cout << "FragTrap destructor\n";
}

void FragTrap::vaulthunter_dot_exe(const std::string& target)
{
    if (_ep < 25)
    {
        std::cout << "FR4G-TP " << _name << " tries to attack " << target
            << " with a vaulthunter_dot_exe, but suffers energy loss."
            << std::endl;
        return;
    }
    _ep -= 25;
    switch (rand() % 5)
    {
        case 0:
            rangedAttack(target);
        break;
        case 1:
            meleeAttack(target);
        break;
        case 2:
            std::cout << "FR4G-TP " << _name << " attacks " << target
                << " with a bunch of stupid jokes, causing headache."
                << std::endl;
        break;
        case 3:
            std::cout << "FR4G-TP " << _name
                << " is depressed and refuses to attack " << target
                << " depressing it as well and causing 15 points of moral damage."
                << std::endl;
        break;
        case 4:
            std::cout << "FR4G-TP " << _name << " attacks " << target
                << " at range, using Meltwodn and Spectre, causing immediate Blue Screen of Death."
                << std::endl;
        break;
        default:
            std::cout << "FR4G-TP " << _name
                << " updates Windows Vista paying no attention to "<< target
                << "." << std::endl;
        break;
    }
}

FragTrap& FragTrap::operator=(const FragTrap& other)
{
    ClapTrap::operator=(other);
    return *this;
}

void FragTrap::Startup()
{
    std::cout << "Starting booting sequence from: " << this
        << "\n\tDirective one: Protect humanity!"
        << "\n\tDirective two: Obey Jack at all costs."
        << "\n\tDirective three: Dance!\n"
        << "Booting sequence complete." << std::endl;
}

void FragTrap::Reset()
{
    std::cout << "Resetting FragTrap stats..." << std::endl;
    _max_hp = 100;
    _max_ep = 100;
    _hp = _max_hp;
    _ep = _max_ep;
    _level = 1;
    _damage_melee = 30;
    _damage_ranged = 20;
    _armor_red = 5;
}
