/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 13:46:55 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 19:31:12 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLAPTRAP_HPP
#define CLAPTRAP_HPP

#include <string>

class ClapTrap
{
public:
    ClapTrap();
    ClapTrap(const std::string& type, const std::string& name = "");
    ClapTrap(const ClapTrap&);
    virtual ~ClapTrap();

    virtual void rangedAttack(const std::string& target) const;
    virtual void meleeAttack(const std::string& target) const;
    virtual void takeDamage(unsigned int amount);
    virtual void beRepaired(unsigned int amount);
    std::string GetString() const;
    ClapTrap& operator=(const ClapTrap&);

protected:
    void Reset();

    unsigned _hp;
    unsigned _max_hp;
    unsigned _ep;
    unsigned _max_ep;
    unsigned _level;
    std::string _type;
    std::string _name;
    unsigned _damage_melee;
    unsigned _damage_ranged;
    unsigned _armor_red;
};

#endif /* CLAPTRAP_HPP */
