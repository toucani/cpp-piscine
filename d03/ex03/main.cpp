/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 09:21:37 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 16:24:24 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"
#include <cstdlib>
#include <iostream>

int main()
{
    srand(time(nullptr));
    ClapTrap* traps[4];

    traps[0] = new ClapTrap("Basic", "Mr. Smith");
    traps[1] = new FragTrap("The first one");
    traps[2] = new ScavTrap("Second one");
    NinjaTrap *ninja = new NinjaTrap("Ninja one");
    traps[3] = ninja;

    std::cout << "=========Ninja attack ctors==========" << std::endl;
    FragTrap ft = FragTrap("Another first one");
    ScavTrap st = ScavTrap("Another scav one");
    NinjaTrap nj = NinjaTrap("Another ninja one");
    std::cout << "=========Ninja attack==========" << std::endl;
    ninja->ninjaShoebox(*traps[0]);
    ninja->ninjaShoebox(ft);
    ninja->ninjaShoebox(st);
    ninja->ninjaShoebox(nj);

    for (unsigned a = 0; a < 4; a++)
    {
        ClapTrap& current = *traps[a];
        std::cout << "=========TEST " << a << "==========" << std::endl;
        current.rangedAttack("dummy target");
        current.rangedAttack("dummy target");
        current.meleeAttack("dummy target");
        current.takeDamage(34);
        current.beRepaired(100500);
        current.takeDamage(50500);
        current.beRepaired(100500);
        delete traps[a];
    }
    return 0;
}
