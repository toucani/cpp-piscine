/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 15:17:40 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 16:18:07 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "NinjaTrap.hpp"
#include <iostream>

NinjaTrap::NinjaTrap() : ClapTrap("NinjaTrap")
{
    std::cout << "NinjaTrap constructor\n";
    Startup();
    Reset();
}

NinjaTrap::NinjaTrap(const std::string& name) : ClapTrap("NinjaTrap", name)
{
    std::cout << "NinjaTrap constructor\n";
    Startup();
    Reset();
}

NinjaTrap::NinjaTrap(const NinjaTrap& other) : ClapTrap(other)
{
    std::cout << "NinjaTrap constructor\n";
}

NinjaTrap::~NinjaTrap()
{
    std::cout << "NinjaTrap destructor\n";
}

void NinjaTrap::ninjaShoebox(const ClapTrap& target)
{
    std::cout << "NinjaTrap attacks " << target.GetString() << std::endl;
}

void NinjaTrap::ninjaShoebox(const NinjaTrap& target)
{
std::cout << "NinjaTrap attacks " << target.GetString() << std::endl;
}

void NinjaTrap::ninjaShoebox(const FragTrap& target)
{
std::cout << "NinjaTrap attacks " << target.GetString() << std::endl;
}

void NinjaTrap::ninjaShoebox(const ScavTrap& target)
{
std::cout << "NinjaTrap attacks " << target.GetString() << std::endl;
}

NinjaTrap& NinjaTrap::operator=(const NinjaTrap& other)
{
    ClapTrap::operator=(other);
    return *this;
}

void NinjaTrap::Startup()
{
    std::cout << "Shhhhh....! This " << this
        << " is not the NinjaTrap you're looking for."
        << std::endl;
}

void NinjaTrap::Reset()
{
    std::cout << "Resetting NinjaTrap stats..." << std::endl;
    _max_hp = 60;
    _max_ep = 120;
    _hp = _max_hp;
    _ep = _max_ep;
    _level = 1;
    _damage_melee = 60;
    _damage_ranged = 5;
    _armor_red = 0;
}