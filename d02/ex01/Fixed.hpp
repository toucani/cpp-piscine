/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 08:56:58 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/04 12:44:25 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
#define FIXED_HPP

#include <string>

class Fixed
{
public:
    Fixed();
    Fixed(const int);
    Fixed(const float);
    Fixed(const Fixed&);
    ~Fixed();

    int getRawBits() const;
    void setRawBits(int raw);
    float toFloat() const;
    int toInt() const;
    Fixed& operator=(const Fixed&);

private:
    void PrintMessage(const std::string&) const;

    int value;
    static const int fractional_bits = 8;
};

std::ostream& operator<<(std::ostream&, const Fixed&);

#endif /* FIXED_HPP */
