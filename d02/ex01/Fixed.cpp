/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 08:57:01 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/04 14:08:41 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Fixed.hpp"
#include <cmath>

Fixed::Fixed()
{
    PrintMessage("default constructor called.");
    value = 0;
}

Fixed::Fixed(const int val)
{
    PrintMessage("int constructor called.");
    value = val << fractional_bits;
}

Fixed::Fixed(const float val)
{
    PrintMessage("float constructor called.");
    value = roundf(val * (1 << fractional_bits));
}

Fixed::Fixed(const Fixed& other)
{
    PrintMessage("copy constructor called.");
    *this = other;
}

Fixed::~Fixed()
{
    PrintMessage("destructor called.");
}

int Fixed::toInt() const
{
    return value >> fractional_bits;
}

float Fixed::toFloat() const
{
    return float(value) / (1 << fractional_bits);
}

int Fixed::getRawBits() const
{
    PrintMessage("getRawBits() const called.");
    return value;
}

void Fixed::setRawBits(const int raw)
{
    PrintMessage("setRawBits(const int raw) called.");
    value = raw;
}

Fixed& Fixed::operator=(const Fixed& other)
{
    PrintMessage("assignment operator called.");
    value = other.value;
    return *this;
}

void Fixed::PrintMessage(const std::string& msg) const
{
    std::cout << static_cast<const void*>(this) << ": " << msg << std::endl;
}

std::ostream& operator<<(std::ostream& os, const Fixed& fx)
{
    os << fx.toFloat();
    return os;
}
