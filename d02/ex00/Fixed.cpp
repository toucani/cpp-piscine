/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 08:57:01 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/04 12:43:12 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Fixed.hpp"

Fixed::Fixed()
{
    PrintMessage("default constructor called.");
    setRawBits(0);
}

Fixed::Fixed(const Fixed& other)
{
    PrintMessage("copy constructor called.");
    *this = other;
}

Fixed::~Fixed()
{
    PrintMessage("destructor called.");
}

int Fixed::getRawBits() const
{
    PrintMessage("getRawBits() const called.");
    return value;
}

void Fixed::setRawBits(const int raw)
{
    PrintMessage("setRawBits(const int raw) called.");
    value = raw;
}

Fixed& Fixed::operator=(const Fixed& other)
{
    PrintMessage("assignment operator called.");
    setRawBits(other.getRawBits());
    return *this;
}

void Fixed::PrintMessage(const std::string& msg) const
{
    std::cout << static_cast<const void*>(this) << ": " << msg << std::endl;
}
