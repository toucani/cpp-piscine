/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 08:56:58 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/04 12:42:21 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
#define FIXED_HPP

#include <string>

class Fixed
{
public:
    Fixed();
    Fixed(const Fixed& other);
    ~Fixed();

    int getRawBits() const;
    void setRawBits(int raw);

    Fixed& operator=(const Fixed& other);

private:
    void PrintMessage(const std::string& msg) const;

    int value;
    static const int fractional_bits = 8;
};

#endif /* FIXED_HPP */
