/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 08:56:58 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 11:43:05 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
#define FIXED_HPP

#include <string>

class Fixed
{
public:
    Fixed();
    Fixed(const int);
    Fixed(const float);
    Fixed(const Fixed&);
    ~Fixed();

    void setValue(int);
    void setValue(float);
    int getRawBits() const;
    void setRawBits(int);
    float toFloat() const;
    int toInt() const;

    Fixed& operator=(const Fixed&);
    Fixed operator+(const Fixed&);
    Fixed operator-(const Fixed&);
    Fixed operator*(const Fixed&);
    Fixed operator/(const Fixed&);

    Fixed& operator++();
    Fixed operator++(int);//post
    Fixed& operator--();
    Fixed operator--(int);//post
    
    bool operator>(const Fixed&) const;
    bool operator>=(const Fixed&) const;
    bool operator<(const Fixed&) const;
    bool operator<=(const Fixed&) const;
    bool operator==(const Fixed&) const;
    bool operator!=(const Fixed&) const;

    static Fixed& min(Fixed&, Fixed&);
    static Fixed& max(Fixed&, Fixed&);
    static const Fixed& min(const Fixed&, const Fixed&);
    static const Fixed& max(const Fixed&, const Fixed&);

private:
    void PrintMessage(const std::string&) const;

    int value;
    static const int fractional_bits = 8;
};

std::ostream& operator<<(std::ostream&, const Fixed&);

#endif /* FIXED_HPP */
