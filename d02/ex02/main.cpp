#include <iostream>
#include "Fixed.hpp"

static void SubjectTests()
{
    std::cout << "==========Subject tests==========" << std::endl;
    Fixed a;
    Fixed const b( Fixed( 5.05f ) * Fixed( 2 ) );
    std::cout << a << std::endl;
    std::cout << ++a << std::endl;
    std::cout << a << std::endl;
    std::cout << a++ << std::endl;
    std::cout << a << std::endl;
    std::cout << b << std::endl;
    std::cout << Fixed::max( a, b ) << std::endl;
    std::cout << "========Subject tests end========" << std::endl;
}

int main( void )
{
    //Code from subject
    SubjectTests();

    const float fa = 4.674f;
    const float fb = 1.9801f;

    std::cout << "\n\nFloats are: " << fa << ", " << fb << std::endl;
    Fixed a(fa), b(fb);
    std::cout << "Fixed are: " << a << ", " << b << std::endl;
    std::cout << "Are they equal: " << (a == b ? "yes" : "no") << std::endl;
    std::cout << "Is a >= b: " << (a >= b ? "yes" : "no") << std::endl;
    std::cout << "Is a > b: " << (a > b ? "yes" : "no") << std::endl;
    std::cout << "Is a < b: " << (a < b ? "yes" : "no") << std::endl;
    std::cout << "Is a <= b: " << (a <= b ? "yes" : "no") << std::endl;
    std::cout << "Min is: " << Fixed::min(a, b) << ", max is: " << Fixed::max(a, b) << std::endl;
    std::cout << "A = " << a << ". A++ = " << a++ << std::endl;
    std::cout << "A = " << a << ". ++A = " << ++a << std::endl;
    std::cout << "A = " << a << ". A + 76.8f = " << a + 76.8f << std::endl;
    std::cout << "A = " << a << ". A-- = " << a-- << std::endl;
    std::cout << "A = " << a << ". --A = " << --a << std::endl;
    std::cout << "A = " << a << ". A - 76.8f = " << a - 76.8f << std::endl;
    std::cout << "A = " << a << ". A * 2 = " << a * 2 << std::endl;
    std::cout << "A = " << a << ". A / 2 = " << a / 2 << std::endl;
    return 0;
}
