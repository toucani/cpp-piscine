/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 08:57:01 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/05 16:54:42 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Fixed.hpp"
#include <cmath>

Fixed::Fixed()
{
    PrintMessage("default constructor called.");
    setValue(0);
}

Fixed::Fixed(const int val)
{
    PrintMessage("int constructor called.");
    setValue(val);
}

Fixed::Fixed(const float val)
{
    PrintMessage("float constructor called.");
    setValue(val);
}

Fixed::Fixed(const Fixed& other)
{
    PrintMessage("copy constructor called.");
    *this = other;
}

Fixed::~Fixed()
{
    PrintMessage("destructor called.");
}

void Fixed::setValue(const int val)
{
    value = val << fractional_bits;
}

void Fixed::setValue(const float val)
{
    value = roundf(val * (1 << fractional_bits));
}

int Fixed::toInt() const
{
    return value >> fractional_bits;
}

float Fixed::toFloat() const
{
    return float(value) / (1 << fractional_bits);
}

int Fixed::getRawBits() const
{
    PrintMessage("getRawBits() const called.");
    return value;
}

void Fixed::setRawBits(const int raw)
{
    PrintMessage("setRawBits(const int raw) called.");
    value = raw;
}

Fixed& Fixed::operator=(const Fixed& other)
{
    PrintMessage("assignment operator called.");
    value = other.value;
    return *this;
}

Fixed Fixed::operator+(const Fixed& other)
{
    return Fixed(toFloat() + other.toFloat());
}

Fixed Fixed::operator-(const Fixed& other)
{
    return Fixed(toFloat() - other.toFloat());
}

Fixed Fixed::operator*(const Fixed& other)
{
    return Fixed(toFloat() * other.toFloat());
}

Fixed Fixed::operator/(const Fixed& other)
{
    return Fixed(toFloat() / other.toFloat());
}

Fixed& Fixed::operator++()
{
    value += 1;
    return *this;
}

Fixed Fixed::operator++(int)
{
    Fixed tmp(*this);
    operator++();
    return tmp;
}

Fixed& Fixed::operator--()
{
    value -= 1;
    return *this;
}

Fixed Fixed::operator--(int)
{
    Fixed tmp(*this);
    operator--();
    return tmp;
}

bool Fixed::operator>(const Fixed& other) const
{
    return value > other.value;
}

bool Fixed::operator>=(const Fixed& other) const
{
    return value >= other.value;
}

bool Fixed::operator<(const Fixed& other) const
{
    return value < other.value;
}

bool Fixed::operator<=(const Fixed& other) const
{
    return value <= other.value;
}

bool Fixed::operator==(const Fixed& other) const
{
    return value == other.value;
}

bool Fixed::operator!=(const Fixed& other) const
{
    return value != other.value;
}

#ifdef LOG
void Fixed::PrintMessage(const std::string& msg) const
{
    std::cout << this << ": " << msg << std::endl;
#else
void Fixed::PrintMessage(const std::string&) const
{
#endif
}

Fixed& Fixed::min(Fixed& left, Fixed& right)
{
    return left < right ? left : right;
}

Fixed& Fixed::max(Fixed& left, Fixed& right)
{
    return left > right ? left : right;
}

const Fixed& Fixed::min(const Fixed& left, const Fixed& right)
{
    return left < right ? left : right;
}

const Fixed& Fixed::max(const Fixed& left, const Fixed& right)
{
    return left > right ? left : right;
}

std::ostream& operator<<(std::ostream& os, const Fixed& fx)
{
    os << fx.toFloat();
    return os;
}
