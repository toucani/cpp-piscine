/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 10:00:44 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/09 17:39:04 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Intern.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "Bureaucrat.hpp"
#include "OfficeBlock.hpp"

int main()
{
    srand(time(0));
    Intern idiotOne;
    Bureaucrat hermes = Bureaucrat("Hermes Conrad", 37);
    Bureaucrat bob = Bureaucrat("Bobby Bobson", 123);
    OfficeBlock ob;
    ob.setIntern(idiotOne);
    ob.setSigner(bob);

    try
    {
        ob.doBureaucracy("mutant pig termination", "Pigley");
    }
    catch (std::exception & e)
    {
        std::cout << "WAT??? Error??? " << e.what() << std::endl;
    }

    std::cout << "\n\nSetting executor...\n";
    ob.setExecutor(hermes);
    try
    {
        ob.doBureaucracy("mutant pig termination", "Pigley");
    }
    catch (std::exception & e)
    {
        std::cout << "WAT??? Error??? " << e.what() << std::endl;
    }

    std::cout << "\n\nCreating normal form...\n";
    ob.setExecutor(hermes);
    try
    {
        ob.doBureaucracy("robotomy request", "Pigley");
    }
    catch (std::exception & e)
    {
        std::cout << "WAT??? Error??? " << e.what() << std::endl;
    }

    std::cout << "\n\nChanging grade...\n";
    bob.setGrade(70);
    try
    {
        ob.doBureaucracy("robotomy request", "Pigley");
    }
    catch (std::exception & e)
    {
        std::cout << "WAT??? Error??? " << e.what() << std::endl;
    }
}
