/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 09:36:58 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/09 14:05:07 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Bureaucrat.hpp"

int main()
{
    try
    {
        Bureaucrat br("Simple guy", 15);
        std::cout << br;
        std::cout << "\t\tIncrement and decrement tests" << std::endl;
        std::cout << br++;
        std::cout << br--;
        std::cout << br;
        std::cout << ++br;
        std::cout << --br;
        std::cout << br;
        std::cout << "\t\tSetter tests" << std::endl;
        br.setGrade(40);
        std::cout << br;
        br.setGrade(2);
        std::cout << br;
        std::cout << "\t\tGetReady!!!" << std::endl;
        br.setGrade(500);
    }
    catch(std::exception& e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cout << "Some unknown exception has been caught..." << std::endl;
    }

    try
    {
        std::cout << "\t\tGetReady!!!" << std::endl;
        Bureaucrat("Another guy", 0);
    }
    catch(std::exception& e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cout << "Some unknown exception has been caught..." << std::endl;
    }
    return 0;
}
