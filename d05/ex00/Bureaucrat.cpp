/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 09:37:12 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/09 09:37:18 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat() : _grade(GradeMin), _name("")
{
}

Bureaucrat::Bureaucrat(const std::string& name, unsigned grade)
    : _grade(GradeMin), _name(name)
{
    setGrade(grade);
}

Bureaucrat::Bureaucrat(const Bureaucrat& other)
    : _name (other._name)
{
    setGrade(other._grade);
}

Bureaucrat::~Bureaucrat()
{
}

Bureaucrat::GradeTooHighException::GradeTooHighException() : std::exception() {}
Bureaucrat::GradeTooHighException::GradeTooHighException(const Bureaucrat::GradeTooHighException& other)
    : std::exception(other) {}
Bureaucrat::GradeTooHighException::~GradeTooHighException() throw() {}

const char* Bureaucrat::GradeTooHighException::what() const throw()
{
    return "Grade is too high!";
}

Bureaucrat::GradeTooHighException& Bureaucrat::GradeTooHighException::operator=(const Bureaucrat::GradeTooHighException& other)
{
    std::exception::operator=(other);
    return *this;
}

Bureaucrat::GradeTooLowException::GradeTooLowException() : std::exception() {}
Bureaucrat::GradeTooLowException::GradeTooLowException(const Bureaucrat::GradeTooLowException& other)
    : std::exception(other) {}
Bureaucrat::GradeTooLowException::~GradeTooLowException() throw() {}

const char* Bureaucrat::GradeTooLowException::what() const throw()
{
    return "Grade is too low!";
}

Bureaucrat::GradeTooLowException& Bureaucrat::GradeTooLowException::operator=(const Bureaucrat::GradeTooLowException& other)
{
    std::exception::operator=(other);
    return *this;
}

void Bureaucrat::setGrade(unsigned value)
{
    if (value > GradeMin)
        throw GradeTooHighException();
    else if (value < GradeMax)
        throw GradeTooLowException();
    else
        _grade = value;
}

unsigned Bureaucrat::getGrade() const
{
    return _grade;
}

const std::string& Bureaucrat::getName() const
{
    return _name;
}

Bureaucrat Bureaucrat::operator++(int)
{
    Bureaucrat tmp(*this);
    setGrade(_grade - 1);
    return tmp;
}

Bureaucrat& Bureaucrat::operator++()
{
    setGrade(_grade - 1);
    return *this;
}

Bureaucrat Bureaucrat::operator--(int)
{
    Bureaucrat tmp(*this);
    setGrade(_grade + 1);
    return tmp;
}

Bureaucrat& Bureaucrat::operator--()
{
    setGrade(_grade + 1);
    return *this;
}

Bureaucrat& Bureaucrat::operator=(const Bureaucrat& other)
{
    setGrade(other._grade);
    return *this;
}

std::ostream& operator<<(std::ostream& os, const Bureaucrat& b)
{
    os << b.getName() << ", bureaucrat grade " << b.getGrade() << std::endl;
    return os;
}
