/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 10:00:44 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 15:36:38 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "CentralBureaucracy.hpp"

int main()
{
    srand(time(0));
    Bureaucrat* burs[20];
    CentralBureaucracy bur;

    burs[0] = new Bureaucrat("Hermes Conrad", 3);
    burs[1] = new Bureaucrat("Bobby Bobson", 3);
    burs[2] = new Bureaucrat("Arnold", 3);
    burs[3] = new Bureaucrat("Mr. Smith", 3);
    burs[4] = new Bureaucrat("Ernest Hemingway", 3);
    burs[5] = new Bureaucrat("Marilyn Monroe", 3);
    burs[6] = new Bureaucrat("Abraham Lincoln", 3);
    burs[7] = new Bureaucrat("Winston Churchill", 3);
    burs[8] = new Bureaucrat("Nelson Mandela", 3);
    burs[9] = new Bureaucrat("Bill Gates", 3);
    burs[10] = new Bureaucrat("Donald Trump", 3);
    burs[11] = new Bureaucrat("Muhammad Ali", 3);
    burs[12] = new Bureaucrat("Mahatma Gandhi", 3);
    burs[13] = new Bureaucrat("Margaret Thatcher", 3);
    burs[14] = new Bureaucrat("Christopher Columbus", 3);
    burs[15] = new Bureaucrat("Rosa Parks", 3);
    burs[16] = new Bureaucrat("Aung San Suu Kyi", 3);
    burs[17] = new Bureaucrat("Lyndon Johnson", 3);
    burs[18] = new Bureaucrat("Oprah Winfrey", 3);
    burs[19] = new Bureaucrat("Benazir Bhutto", 3);

    for (int i = 0; i < 20; i++)
        bur.pushBack(*burs[i]);

    bur.pushBack("Isaias Ho");
    bur.pushBack("Tommy Brandt");
    bur.pushBack("Marley Bright");
    bur.pushBack("Lacey Murray");
    bur.pushBack("Dexter Nicholson");
    bur.pushBack("Carley Carrillo");
    bur.pushBack("Lindsey Montoya");
    bur.pushBack("Alejandro Chandler");
    bur.pushBack("Paula Lamb");
    bur.pushBack("Hazel Snyder");
    bur.pushBack("Jasper Reyes");
    bur.pushBack("Alicia Francis");
    bur.pushBack("Pamela Dunlap");
    bur.pushBack("Madden Russell");
    bur.pushBack("Claire Smith");
    bur.pushBack("Jovany Robertson");
    bur.pushBack("Reilly Johns");
    bur.pushBack("Caylee Morrow");
    bur.pushBack("Larry Hickman");
    bur.pushBack("Alissa Holt");
    bur.pushBack("Nathaniel Le");
    bur.pushBack("Parker Mullen");
    bur.pushBack("Diana Zamora");
    bur.pushBack("Kamden Burch");
    bur.pushBack("Jaylee Sims");
    bur.pushBack("Andy Gilmore");
    bur.pushBack("Tristian Griffin");
    bur.pushBack("Cornelius Hines");
    bur.pushBack("Scarlett Trevino");
    bur.pushBack("Leandro Rich");
    bur.pushBack("Kyla Schultz");
    bur.pushBack("Sidney Johnston");
    bur.pushBack("Agustin Ware");
    bur.pushBack("Sheldon Avery");
    bur.pushBack("Blaze Hutchinson");
    bur.pushBack("Abby Thompson");
    bur.pushBack("Tyler Miranda");
    bur.pushBack("Payton Grant");
    bur.pushBack("Amber Norman");
    bur.pushBack("Hannah May");

    try
    {
        bur.doBureaucracy();
    }
    catch (std::exception & e)
    {
        std::cout << "WAT??? Error??? " << e.what() << std::endl;
    }
}
