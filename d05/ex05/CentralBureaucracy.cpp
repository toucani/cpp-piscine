/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   CentralBureaucracy.cpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 13:15:12 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 15:37:24 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "CentralBureaucracy.hpp"
#include <iostream>

CentralBureaucracy::CentralBureaucracy() : _currentEmpty(0) {}
CentralBureaucracy::CentralBureaucracy(const CentralBureaucracy&) {}
CentralBureaucracy::~CentralBureaucracy() {}
CentralBureaucracy& CentralBureaucracy::operator=(const CentralBureaucracy&) { return *this;}

void CentralBureaucracy::pushBack(Bureaucrat& b)
{
    if (_currentEmpty >= blocksAmount)
        return;

    OfficeBlock& current = _blocks[_currentEmpty];

    if (!current.hasIntern())
        current.setIntern(_intern);
    if (!current.hasSigner())
        current.setSigner(b);
    else if (!current.hasExecutor())
    {
        current.setExecutor(b);
        _currentEmpty++;
    }
}

void CentralBureaucracy::pushBack(const std::string& target)
{
    _targets.push(target);
}

void CentralBureaucracy::doBureaucracy()
{
    unsigned currentOfficeId = 0;
    while (!_targets.empty())
    {
        _blocks[currentOfficeId].doBureaucracy((rand() % 2) == 1 ? "robotomy request" : "presidential pardon", _targets.front());
        _targets.pop();
        currentOfficeId = currentOfficeId + 1 >= blocksAmount ? 0 : currentOfficeId + 1;
        std::cout << std::endl;
    }
}