/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OfficeBlock.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 17:10:42 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 14:14:15 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OfficeBlock.hpp"
#include <iostream>

OfficeBlock::OfficeBlock()
: _intern(0), _signer(0), _executioner(0) {}

OfficeBlock::OfficeBlock(Intern& intern, Bureaucrat& signer, Bureaucrat& executioner)
    : _intern(&intern), _signer(&signer), _executioner(&executioner) {}

OfficeBlock::Shortage::Shortage() : std::exception() {}
OfficeBlock::Shortage::Shortage(const std::string& msg)
    : std::exception(), _msg(msg) {}
OfficeBlock::Shortage::Shortage(const OfficeBlock::Shortage& other)
    : std::exception(other), _msg(other._msg) {}
OfficeBlock::Shortage::~Shortage() throw() {}
const char* OfficeBlock::Shortage::what() const throw()
{
    return _msg.c_str();
}

OfficeBlock::Shortage& OfficeBlock::Shortage::operator=(const OfficeBlock::Shortage& other)
{
    std::exception::operator=(other);
    return *this;
}

bool OfficeBlock::hasIntern() const { return _intern != 0; }
bool OfficeBlock::hasSigner() const { return _signer != 0; }
bool OfficeBlock::hasExecutor() const { return _executioner != 0; }

void OfficeBlock::setIntern(Intern& i)
{
    _intern = &i;
}

void OfficeBlock::setSigner(Bureaucrat& b)
{
    _signer = &b;
}

void OfficeBlock::setExecutor(Bureaucrat& b)
{
    _executioner = &b;
}

void OfficeBlock::doBureaucracy(const std::string& form, const std::string& target)
{
    if (!_intern)
        throw Shortage("Intern is missing!");
    if (!_signer)
        throw Shortage("There is noone to sign the form!");
    if (!_executioner)
        throw Shortage("There is noone to execute the form!");

    Form *f = _intern->makeForm(form, target);
    if (!f)
        throw Shortage("No form - no target!");
    f->beSigned(*_signer);
    f->execute(*_executioner);
    std::cout << "That will do " << target <<", that will do..." << std::endl;
}