/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   CentralBureaucracy.hpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 13:15:09 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 14:13:46 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CENTRALBUREAUCRACY_HPP
#define CENTRALBUREAUCRACY_HPP

#include "OfficeBlock.hpp"
#include <queue>

class CentralBureaucracy
{
public:
    CentralBureaucracy();
    CentralBureaucracy(const CentralBureaucracy&);
    ~CentralBureaucracy();
    CentralBureaucracy& operator=(const CentralBureaucracy&);

    static const unsigned blocksAmount = 10;

    void pushBack(Bureaucrat&);
    void pushBack(const std::string& target);
    void doBureaucracy();

private:
    Intern _intern;
    OfficeBlock _blocks[blocksAmount];
    std::queue<std::string> _targets;
    unsigned _currentEmpty;
};

#endif /* CENTRALBUREAUCRACY_HPP */
