/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OfficeBlock.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 17:06:43 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 13:41:09 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OFFICEBLOCK_HPP
#define OFFICEBLOCK_HPP

#include "Intern.hpp"
#include "Bureaucrat.hpp"
#include <exception>

class OfficeBlock
{
public:
    OfficeBlock();
    OfficeBlock(Intern& intern, Bureaucrat& signer, Bureaucrat& executioner);

    class Shortage : public std::exception
    {
    public:
        Shortage();
        Shortage(const std::string& msg);
        Shortage(const Shortage&);
        ~Shortage() throw();
        const char* what() const throw();
        Shortage& operator=(const Shortage&);

    private:
        std::string _msg;
    };

    bool hasIntern() const;
    bool hasSigner() const;
    bool hasExecutor() const;

    void setIntern(Intern&);
    void setSigner(Bureaucrat&);
    void setExecutor(Bureaucrat&);
    void doBureaucracy(const std::string& form, const std::string& target);

private:
    Intern *_intern;
    Bureaucrat *_signer;
    Bureaucrat *_executioner;
};

#endif /* OFFICEBLOCK_HPP */
