/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 10:00:54 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/09 14:32:38 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUREAUCRAT_HPP
#define BUREAUCRAT_HPP

#include <string>
#include <ostream>
#include <exception>

class Form;

class Bureaucrat
{
public:
    Bureaucrat();
    Bureaucrat(const std::string& name, unsigned grade);
    Bureaucrat(const Bureaucrat&);
    ~Bureaucrat();

    class GradeTooHighException : public std::exception
    {
    public:
        GradeTooHighException();
        GradeTooHighException(const GradeTooHighException&);
        ~GradeTooHighException() throw();
        const char* what() const throw();
        GradeTooHighException& operator=(const GradeTooHighException&);
    };

    class GradeTooLowException : public std::exception
    {
    public:
        GradeTooLowException();
        GradeTooLowException(const GradeTooLowException&);
        ~GradeTooLowException() throw();
        const char* what() const throw();
        GradeTooLowException& operator=(const GradeTooLowException&);
    };

    void signForm(Form&);
    void setGrade(unsigned);
    unsigned getGrade() const;
    const std::string& getName() const;

    void executeForm(const Form&);

    static const unsigned GradeMax = 1;
    static const unsigned GradeMin = 150;

    Bureaucrat operator++(int);
    Bureaucrat& operator++();
    Bureaucrat operator--(int);
    Bureaucrat& operator--();

    Bureaucrat& operator=(const Bureaucrat&);

private:
    unsigned _grade;
    const std::string _name;
};

std::ostream& operator<<(std::ostream&, const Bureaucrat&);

#endif /* BUREAUCRAT_HPP */
