/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 10:00:48 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/09 14:10:13 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FORM_HPP
#define FORM_HPP

#include <string>
#include <ostream>
#include <exception>
#include "Bureaucrat.hpp"

class Form
{
public:
    Form();
    Form(const std::string& name, const std::string& target = "", unsigned gradeSign = Bureaucrat::GradeMin, unsigned gradeExe = Bureaucrat::GradeMin);
    Form(const Form&);
    virtual ~Form();

    class GradeTooHighException : public std::exception
    {
    public:
        GradeTooHighException();
        GradeTooHighException(const GradeTooHighException&);
        ~GradeTooHighException() throw();
        const char* what() const throw();
        GradeTooHighException& operator=(const GradeTooHighException&);
    };

    class GradeTooLowException : public std::exception
    {
    public:
        GradeTooLowException();
        GradeTooLowException(const GradeTooLowException&);
        ~GradeTooLowException() throw();
        const char* what() const throw();
        GradeTooLowException& operator=(const GradeTooLowException&);
    };

    class NotSigned : public std::exception
    {
    public:
        NotSigned();
        NotSigned(const NotSigned&);
        ~NotSigned() throw();
        const char* what() const throw();
        NotSigned& operator=(const NotSigned&);
    };

    const std::string& getName() const;
    bool isSigned() const;
    void beSigned(const Bureaucrat&);
    unsigned getGradeExe() const;
    unsigned getGradeSign() const;
    void execute(const Bureaucrat&) const;
    Form& operator=(const Form&);

protected:
    virtual void execute() const = 0;

    const std::string _name;
    const unsigned _gradeSign;
    const unsigned _gradeExecute;
    bool _isSigned;
    std::string _target;
};

std::ostream& operator<<(std::ostream&, const Form&);

#endif /* FORM_HPP */
