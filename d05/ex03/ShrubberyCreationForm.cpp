/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.cpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 12:57:03 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 13:05:10 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ShrubberyCreationForm.hpp"
#include <iostream>
#include <fstream>

ShrubberyCreationForm::ShrubberyCreationForm()
    : Form("ShrubberyCreationForm", "default target", 145, 137) {}

ShrubberyCreationForm::ShrubberyCreationForm(const std::string& target)
    : Form("ShrubberyCreationForm", target, 145, 137) {}

ShrubberyCreationForm::ShrubberyCreationForm(const ShrubberyCreationForm& other)
    : Form(other) {}

ShrubberyCreationForm::~ShrubberyCreationForm() {}

void ShrubberyCreationForm::execute() const
{
    std::fstream file(_target + "_shrubbery", std::fstream::out | std::fstream::trunc);

    if (!file.is_open() || !file.good())
    {
        std::cout << "Fail! Fail! Fail creating " << _target
        << "_shrubbery file!\nNo ASCII trees have been written!" << std::endl;
        return;
    }

    //Freaking trees
    file << "\
                            '.,\n\
                              'b      *\n\
                               '$    #.\n\
                                $:   #:\n\
                                *#  @):\n\
                                :@,@):   ,.**:'\n\
                      ,         :@@*: ..**'\n\
                       '#o.    .:(@'.@*\"'\n\
                          'bq,..:,@@*'   ,*\n\
                          ,p$q8,:@)'  .p*'\n\
                         '    '@@Pp@@*'\n\
                               Y7'.'\n\
                              :@):.\n\
                             .:@:'.\n\
                           .::(@:.      -Sam Blumenstein-\n\n\n";

    file << "\
                          . . .\n\
                   .        .  .     ..    .\n\
                .                 .         .  .\n\
                                .\n\
                               .                ..\n\
               .          .            .              .\n\
               .            '.,        .               .\n\
               .              'b      *\n\
                .              '$    #.                ..\n\
               .    .           $:   #:               .\n\
             ..      .  ..      *#  @):        .   . .\n\
                          .     :@,@):   ,.**:'   .\n\
              .      .,         :@@*: ..**'      .   .\n\
                       '#o.    .:(@'.@*\"'  .\n\
               .  .       'bq,..:,@@*'   ,*      .  .\n\
                          ,p$q8,:@)'  .p*'      .\n\
                   .     '  . '@@Pp@@*'    .  .\n\
                    .  . ..    Y7'.'     .  .\n\
                              :@):.\n\
                             .:@:'.\n\
                           .::(@:.      -Sam Blumenstein-\n\n\n";

    file << "\
                     - - -\n\
                   -        -  -     --    -\n\
                -                 -         -  -\n\
                                -\n\
                               -                --\n\
               -          -            -              -\n\
               -            '-,        -               -\n\
               -              'b      *\n\
                -              '$    #-                --\n\
               -    -           $:   #:               -\n\
             --      -  --      *#  @):        -   - -\n\
                          -     :@,@):   ,-**:'   -\n\
              -      -,         :@@*: --**'      -   -\n\
                       '#o-    -:(@'-@*\"'  -\n\
               -  -       'bq,--:,@@*'   ,*      -  -\n\
                          ,p$q8,:@)'  -p*'      -\n\
                   -     '  - '@@Pp@@*'    -  -\n\
                    -  - --    Y7'.'     -  -\n\
                              :@):.\n\
                             .:@:'.\n\
                           .::(@:.      -Sam Blumenstein-\n\n\n";

    file << "\
                     / / /\n\
                   /        /  /     //    /\n\
                /                 /         /  /\n\
                                /\n\
                               /                //\n\
               /          /            /              /\n\
               /            '/,        /               /\n\
               /              'b      *\n\
                /              '$    //                //\n\
               /    /           $:   /:               /\n\
             //      /  //      */  @):        /   / /\n\
                          /     :@,@):   ,/**:'   /\n\
              /      /,         :@@*: //**'      /   /\n\
                       '/o/    /:(@'/@*\"'  /\n\
               /  /       'bq,//:,@@*'   ,*      /  /\n\
                          ,p$q8,:@)'  /p*'      /\n\
                   /     '  / '@@Pp@@*'    /  /\n\
                    /  / //    Y7'.'     /  /\n\
                              :@):.\n\
                             .:@:'.\n\
                           .::(@:.      -Sam Blumenstein-\n\n\n";

    file << "\
                      ; ; ;\n\
                   ;        ;  ;     ;;    ;\n\
                ;                 ;         ;  ;\n\
                                ;\n\
                               ;                ;;\n\
               ;          ;            ;              ;\n\
               ;            ';,        ;               ;\n\
               ;              'b      *\n\
                ;              '$    ;;                ;;\n\
               ;    ;           $:   ;:               ;\n\
             ;;      ;  ;;      *;  @):        ;   ; ;\n\
                          ;     :@,@):   ,;**:'   ;\n\
              ;      ;,         :@@*: ;;**'      ;   ;\n\
                       ';o;    ;:(@';@*\"'  ;\n\
               ;  ;       'bq,;;:,@@*'   ,*      ;  ;\n\
                          ,p$q8,:@)'  ;p*'      ;\n\
                   ;     '  ; '@@Pp@@*'    ;  ;\n\
                    ;  ; ;;    Y7'.'     ;  ;\n\
                              :@):.\n\
                             .:@:'.\n\
                           .::(@:.      -Sam Blumenstein-" << std::endl;

    std::cout << _target
        << "_shrubbery file created!\nASCII tree has been written!" << std::endl;
}

ShrubberyCreationForm& ShrubberyCreationForm::operator=(const ShrubberyCreationForm& other)
{
    Form::operator=(other);
    return *this;
}