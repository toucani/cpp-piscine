/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 15:59:48 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/09 16:25:32 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Intern.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include <iostream>

Intern::Intern() {}
Intern::Intern(const Intern&) {}
Intern::~Intern() {}

Form *Intern::makeForm(const std::string& name, const std::string& target)
{
    std::cout << "Intern tries to create " << name << std::endl;

    if (name.find("presidential pardon") != std::string::npos)
        return new PresidentialPardonForm(target);
    else if (name.find("shrubbery creation") != std::string::npos)
        return new ShrubberyCreationForm(target);
    else if (name.find("robotomy request") != std::string::npos)
        return new RobotomyRequestForm(target);

    std::cout << "Intern cannot create " << name << "form." << std::endl;
    return 0;
}

Intern& Intern::operator=(const Intern&) { return *this; }