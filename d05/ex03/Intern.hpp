/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 15:57:31 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/09 16:23:45 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INTERN_HPP
#define INTERN_HPP

#include <string>
#include "Form.hpp"

class Intern
{
public:
    Intern();
    Intern(const Intern&);
    ~Intern();
    Form *makeForm(const std::string& name, const std::string& target);
    Intern& operator=(const Intern&);
};

#endif /* INTERN_HPP */
