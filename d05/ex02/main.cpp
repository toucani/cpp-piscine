/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 10:00:44 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/09 14:56:28 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "Bureaucrat.hpp"

static void TestB()
{
    Bureaucrat br("Albert", 2);
    PresidentialPardonForm fr("Mr. Smith");

    std::cout << "\n====   INFO   ====\n" << br << fr << "====   EXC   ====\n";
    br.executeForm(fr);
    std::cout << "\n====   INFO   ====\n" << br << fr << "====   EXC   ====\n";
    br.signForm(fr);
    std::cout << "\n====   INFO   ====\n" << br << fr << "====   EXC   ====\n";
    br.executeForm(fr);
    std::cout << "\n====   INFO   ====\n" << br << fr << "====   EXC   ====\n";
}

static void TestF()
{
    Bureaucrat br("Albert", 100);
    ShrubberyCreationForm fr("Government");

    try
    {
        fr.beSigned(br);
    }
    catch(std::exception& e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cout << "Some unknown exception has been caught..." << std::endl;
    }

    try
    {
        Bureaucrat br("The one", 5);
        fr.beSigned(br);
    }
    catch(std::exception& e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cout << "Some unknown exception has been caught..." << std::endl;
    }

    try
    {
        Bureaucrat br("The one", 5);
        (static_cast<Form&>(fr)).execute(br);
    }
    catch(std::exception& e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cout << "Some unknown exception has been caught..." << std::endl;
    }
}

int main()
{
    srand(time(0));
    TestB();
    std::cout << "\n" << std::endl;
    TestF();
    return 0;
}
