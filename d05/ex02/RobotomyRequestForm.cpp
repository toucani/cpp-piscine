/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 13:28:37 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 12:49:05 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RobotomyRequestForm.hpp"
#include <iostream>

RobotomyRequestForm::RobotomyRequestForm()
    : Form("RobotomyRequestForm", "default target", 72, 45) {}

RobotomyRequestForm::RobotomyRequestForm(const std::string& target)
    : Form("RobotomyRequestForm", target, 72, 45) {}

RobotomyRequestForm::RobotomyRequestForm(const RobotomyRequestForm& other)
    : Form(other) {}

RobotomyRequestForm::~RobotomyRequestForm() {}

void RobotomyRequestForm::execute() const
{
    std::cout << "* DRILLING SOUND *\n";
    if ((rand() % 2) == 1)
        std::cout << _target << " has been robotomized successfully!" << std::endl;
    else
        std::cout << "Fail! Fail! Fail trying robotomize " << _target << std::endl;
}

RobotomyRequestForm& RobotomyRequestForm::operator=(const RobotomyRequestForm& other)
{
    Form::operator=(other);
    return *this;
}