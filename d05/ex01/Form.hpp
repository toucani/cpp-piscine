/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 10:00:48 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/09 10:00:49 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FORM_HPP
#define FORM_HPP

#include <string>
#include <ostream>
#include <exception>
#include "Bureaucrat.hpp"

class Form
{
public:
    Form();
    Form(const std::string& name, unsigned gradeSign = Bureaucrat::GradeMin, unsigned gradeExe = Bureaucrat::GradeMin);
    Form(const Form&);
    ~Form();

    class GradeTooHighException : public std::exception
    {
    public:
        GradeTooHighException();
        GradeTooHighException(const GradeTooHighException&);
        ~GradeTooHighException() throw();
        const char* what() const throw();
        GradeTooHighException& operator=(const GradeTooHighException&);
    };

    class GradeTooLowException : public std::exception
    {
    public:
        GradeTooLowException();
        GradeTooLowException(const GradeTooLowException&);
        ~GradeTooLowException() throw();
        const char* what() const throw();
        GradeTooLowException& operator=(const GradeTooLowException&);
    };

    const std::string& getName() const;
    bool isSigned() const;
    void beSigned(const Bureaucrat&);
    unsigned getGradeExe() const;
    unsigned getGradeSign() const;

    Form& operator=(const Form&);

private:
    const std::string _name;
    const unsigned _gradeSign;
    const unsigned _gradeExecute;
    bool _isSigned;
};

std::ostream& operator<<(std::ostream&, const Form&);

#endif /* FORM_HPP */
