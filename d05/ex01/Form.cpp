/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 10:00:51 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/09 10:00:52 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Form.hpp"

Form::Form()
    : _gradeSign(Bureaucrat::GradeMin), _gradeExecute(Bureaucrat::GradeMin)
    , _isSigned(false)
{
}

Form::Form(const std::string& name, unsigned gradeSign, unsigned gradeExe)
    : _name(name), _gradeSign(gradeSign), _gradeExecute(gradeExe), _isSigned(false)
{
}

Form::Form(const Form& other)
    : _name(other._name), _gradeSign(other._gradeSign)
    , _gradeExecute(other._gradeExecute), _isSigned(other._isSigned)
{
    if (_gradeExecute < Bureaucrat::GradeMax || _gradeSign < Bureaucrat::GradeMax)
        throw GradeTooHighException();
    if (_gradeExecute > Bureaucrat::GradeMin || _gradeSign > Bureaucrat::GradeMin)
        throw GradeTooLowException();
}

Form::~Form()
{
}


Form::GradeTooHighException::GradeTooHighException() : std::exception() {}
Form::GradeTooHighException::GradeTooHighException(const Form::GradeTooHighException& other)
    : std::exception(other) {}
Form::GradeTooHighException::~GradeTooHighException() throw() {}

const char* Form::GradeTooHighException::what() const throw()
{
    return "Grade is too high!";
}

Form::GradeTooHighException& Form::GradeTooHighException::operator=(const Form::GradeTooHighException& other)
{
    std::exception::operator=(other);
    return *this;
}

Form::GradeTooLowException::GradeTooLowException() : std::exception() {}
Form::GradeTooLowException::GradeTooLowException(const Form::GradeTooLowException& other)
    : std::exception(other) {}
Form::GradeTooLowException::~GradeTooLowException() throw() {}

const char* Form::GradeTooLowException::what() const throw()
{
    return "Grade is too low!";
}

Form::GradeTooLowException& Form::GradeTooLowException::operator=(const Form::GradeTooLowException& other)
{
    std::exception::operator=(other);
    return *this;
}

const std::string& Form::getName() const
{
    return _name;
}

bool Form::isSigned() const
{
    return _isSigned;
}

void Form::beSigned(const Bureaucrat& bur)
{
    if (_isSigned)
        return;

    if (bur.getGrade() <= _gradeSign)
        _isSigned = true;
    else
        throw GradeTooLowException();
}

unsigned Form::getGradeExe() const
{
    return _gradeExecute;
}

unsigned Form::getGradeSign() const
{
    return _gradeSign;
}

Form& Form::operator=(const Form& other)
{
    _isSigned = other._isSigned;
    return *this;
}

std::ostream& operator<<(std::ostream& os, const Form& f)
{
    os << "Form " << f.getName() << "(S: " << f.getGradeSign() << ", E: " << f.getGradeExe()
        << ") is " << (f.isSigned() ? "" : "not ") << "signed." << std::endl;
    return os;
}
