/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 10:00:44 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/09 10:00:45 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Form.hpp"
#include "Bureaucrat.hpp"

static void Test1()
{
    Bureaucrat br("Simple guy", 100);
    Form fr("ER34-65w", 100, 40);
    Form fr2("ER34-65f", 60, 40);

    std::cout << br << fr << fr2;
    br.signForm(fr);
    br.signForm(fr2);
}

static void Test2()
{
    Bureaucrat br("Albert", 100);
    Form fr("ER34-65w", 100, 40);
    Form fr2("ER34-65f", 60, 40);

    std::cout << br << fr << fr2;
    try
    {
        fr.beSigned(br);
        std::cout << fr;
        fr2.beSigned(br);
        std::cout << fr2;
    }
    catch(std::exception& e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cout << "Some unknown exception has been caught..." << std::endl;
    }
}

int main()
{
    Test1();
    Test2();
    return 0;
}
