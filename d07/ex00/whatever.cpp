/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   whatever.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 09:10:06 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 09:17:29 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

template <typename T>
void swap(T& arg1, T& arg2)
{
    T arg3 = arg1;
    arg1 = arg2;
    arg2 = arg3;
}

template <typename T>
T& min(T& l, T& r)
{
    return (l < r) ? l : r;
}

template <typename T>
T& max(T& l, T& r)
{
    return (l > r) ? l : r;
}

int main()
{
    int a = 2;
    int b = 3;
    std::cout << "a = " << a << ", b = " << b << ". Aaaand swap!" << std::endl;
    ::swap( a, b );
    std::cout << "a = " << a << ", b = " << b << std::endl;
    std::cout << "min( a, b ) = " << ::min( a, b ) << std::endl;
    std::cout << "max( a, b ) = " << ::max( a, b ) << std::endl;
    std::string c = "chaine1";
    std::string d = "chaine2";
    std::cout << "c = " << c << ", d = " << d << ". Aaaand swap!" << std::endl;
    ::swap(c, d);
    std::cout << "c = " << c << ", d = " << d << std::endl;
    std::cout << "min( c, d ) = " << ::min( c, d ) << std::endl;
    std::cout << "max( c, d ) = " << ::max( c, d ) << std::endl;
    return 0;
}
