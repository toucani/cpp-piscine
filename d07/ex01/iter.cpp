/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iter.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 09:10:06 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 09:26:17 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

template <typename Element>
void iter(Element *arr, size_t length, void (*f)(Element))
{
    for (size_t ct = 0; ct < length && f; ct ++)
        (*f)(arr[ct]);
}

template <typename arg>
void print(arg a)
{
    std::cout << a << " ";
}

int main()
{
    int arrI[5] = { 1, 2, 3, 4, 5};
    long long arrL[5] = { 5, 4, 3, 2, 1};
    std::string arrS[5] = { "1s", "2s", "3s", "4s", "5s"};

    iter(arrI, 5, print);
    std::cout << std::endl;
    iter(arrL, 5, print);
    std::cout << std::endl;
    iter(arrS, 5, print);
    std::cout << std::endl;

    return 0;
}
