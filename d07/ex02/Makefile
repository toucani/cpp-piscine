# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/06 22:31:58 by dkovalch          #+#    #+#              #
#    Updated: 2018/04/10 18:14:14 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

								#################
								#	VARIABLES	#
								#################

NAME			= container
NAME_D			= container_deb

CC				= clang++

STD_FLAGS		= -x c++ -std=c++98
ERR_FLAGS		= -Wall -Werror -Wextra

COMPILE_FLAGS	= $(STD_FLAGS) $(ERR_FLAGS) -D NDEBUG
COMPILE_FLAGS_D	= $(STD_FLAGS) $(ERR_FLAGS) -D DEBUG -g -g3 $(SANITY_FLAGS)

LINKING_FLAGS	= $(ERR_FLAGS)
LINKING_FLAGS_D	= $(ERR_FLAGS) -g -g3 $(SANITY_FLAGS)

SANITY_FLAGS	= -fno-omit-frame-pointer \
					-fsanitize=address,shift,null,signed-integer-overflow \
					-fsanitize=vla-bound,bool,enum

INCLUDES		= -iquote includes

PRINTF_2_ARGS	= @printf "%-15s %-65s\n"
PRINTF_3_ARGS	= @printf "%-15s %-65s%20s\n"


								#################
								#	SOURCES		#
								#################

VPATH	= .

								#################
								#	OBJECTS		#
								#################

OBJECTS_FLD = objects

OBJECTS =\
	$(OBJECTS_FLD)/main.o

OBJECTS_D = $(patsubst %.o, %_D.o, $(OBJECTS))


								#############
								#	RULES	#
								#############

.SILENT :

all : $(NAME)

debug : $(NAME_D)

$(OBJECTS_FLD) :
	mkdir -p $(OBJECTS_FLD)

clean :
	rm -rf $(OBJECTS_FLD)

fclean : clean
	rm -rf $(NAME) $(NAME_D) $(NAME).dSym $(NAME_D).dSym

re : fclean all

dre : fclean debug

	#########################
	#	compilation rules	#
	#########################

$(NAME) : $(OBJECTS_FLD) $(OBJECTS)
	$(PRINTF_2_ARGS) "Li͢nki͞n҉g" $(NAME)
	$(CC) $(INCLUDES) $(LINKING_FLAGS) $(OBJECTS) $(LIBRARIES) -o $(NAME)

$(NAME_D) : $(OBJECTS_FLD) $(OBJECTS_D)
	$(PRINTF_3_ARGS) "Li͢nki͞n҉g" $(NAME_D) "wit͠h̀ ̨śa̵nit̷iz̡eŕs̷"
	$(CC) $(INCLUDES) $(LINKING_FLAGS_D) $(OBJECTS_D) $(LIBRARIES_D) -o $(NAME_D)

	#####################
	#	objects rules	#
	#####################

$(OBJECTS_FLD)/%.o : %.cpp
	$(PRINTF_2_ARGS) "Co̸m͢p͏ili͢ng" $<
	$(CC) $(INCLUDES) $(COMPILE_FLAGS) -c $< -o $@

$(OBJECTS_FLD)/%_D.o : %.cpp
	$(PRINTF_3_ARGS) "Co̸m͢p͏ili͢ng" $< "with sanitizers"
	$(CC) $(INCLUDES) $(COMPILE_FLAGS_D) -c $< -o $@
