/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 09:28:05 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 17:27:19 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Container.hpp"
#include <string>
#include <iostream>

int main()
{
    Container<std::string> c(10);

    for (int i = 0; i < 10; i++)
        c[i] = "The data";
    for (int i = 0; i < 10; i++)
        std::cout << i << ") " << c[i] << std::endl;

    for (int i = 0; i < 10; i++)
        c[i] = "The new data";
    for (int i = 0; i < 10; i++)
        std::cout << i << ") " << c[i] << std::endl;

    return 0;
}
