/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Container.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/10 09:28:26 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/10 18:43:09 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTAINER_HPP
#define CONTAINER_HPP

#include <stdexcept>

template <typename T>
class Container
{
public:
    Container() : _data(0), _size(0) {}
    Container(const Container& other) { *this = other; }
    Container(unsigned size) : _size(size) { _data = new T[_size]; }
    ~Container()
    {
        delete[] _data;
    }

    T& operator[](unsigned id)
    {
        if (id >= _size)
            throw std::out_of_range("Argument is out of range.");
        return _data[id];
    }

    const T& operator[](unsigned id) const
    {
        if (id >= _size)
            throw std::out_of_range("Argument is out of range.");
        return _data[id];
    }

    Container& operator=(const Container& other)
    {
        _size = other._size;
        _data = new T[_size];
        for (unsigned ct = 0; ct < _size; ct++)
            _data[ct] = other._data[ct];
        return *this;
    }

    unsigned size() const { return _size; }

private:
    T *_data;
    unsigned _size;
};

#endif /* CONTAINER_HPP */
