/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/02 22:30:40 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/03 08:09:18 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

void Trim(std::string& str)
{
    const size_t fpos = str.find_first_not_of(" \t\f\v\n\r");
    if (fpos == std::string::npos)
        str.clear();
    else
    {
        str = str.substr(fpos);
        const size_t lpos = str.find_last_not_of(" \t\f\v\n\r");
        if (lpos < str.length())
            str = str.substr(0, lpos + 1);
    }
}

void Uppercase(std::string& str)
{
    for (size_t ct = 0; ct < str.length(); ct++)
    {
        if (str[ct] >= 'a' && str[ct] <= 'z')
            str[ct] -= std::abs('A' - 'a');
    }
}

int main(int ac, char** av)
{
    if (ac == 1)
    {
        std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *";
    }
    for (int str = 1; str < ac && av[str]; str++)
    {
        std::string myStr(av[str]);
        Trim(myStr);
        Uppercase(myStr);
        std::cout << myStr << ' ';
    }
    std::cout << std::endl;
    return 0;
}
