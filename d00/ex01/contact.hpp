/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   contact.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/02 20:06:30 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/03 08:11:02 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#pragma once

#include <iostream>

class contact
{
    public:
        contact();

        std::string first_name;
        std::string last_name;
        std::string nickname;
        std::string login;
        std::string post_addr;
        std::string email;
        std::string phone;
        std::string birthday;
        std::string favor_meal;
        std::string underwear_col;
        std::string secret;
};

std::ostream& operator<<(std::ostream& os, const contact& cont);
std::istream& operator>>(std::istream& is, contact& cont);
