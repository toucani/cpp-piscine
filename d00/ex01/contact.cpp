/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   contact.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/02 20:13:01 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/03 08:52:26 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "contact.hpp"
#include <iomanip>

contact::contact()
{
};

std::ostream& operator<<(std::ostream& os, const contact& cont)
{
    os << std::setw(20) << std::setiosflags(std::ios_base::left) << "First name: " << cont.first_name << std::endl;
    os << std::setw(20) << std::setiosflags(std::ios_base::left) << "Last name: " << cont.last_name << std::endl;
    os << std::setw(20) << std::setiosflags(std::ios_base::left) << "Nickname: " << cont.nickname << std::endl;
    os << std::setw(20) << std::setiosflags(std::ios_base::left) << "Login: " << cont.login << std::endl;
    os << std::setw(20) << std::setiosflags(std::ios_base::left) << "Address: " << cont.post_addr << std::endl;
    os << std::setw(20) << std::setiosflags(std::ios_base::left) << "E-mail: " << cont.email << std::endl;
    os << std::setw(20) << std::setiosflags(std::ios_base::left) << "Phone number: " << cont.phone << std::endl;
    os << std::setw(20) << std::setiosflags(std::ios_base::left) << "Birthday: " << cont.birthday << std::endl;
    os << std::setw(20) << std::setiosflags(std::ios_base::left) << "Favorite meal: " << cont.favor_meal << std::endl;
    os << std::setw(20) << std::setiosflags(std::ios_base::left) << "Underwear color: " << cont.underwear_col << std::endl;
    os << std::setw(20) << std::setiosflags(std::ios_base::left) << "Darkest secret: " << cont.secret << std::endl;
    os << std::resetiosflags(std::ios_base::left);
    return os;
}

std::istream& operator>>(std::istream& is, contact& cont)
{
    std::cout << "First name: "; std::getline(is, cont.first_name);
    std::cout << "Last name: "; std::getline(is, cont.last_name);
    std::cout << "Nickname: "; std::getline(is, cont.nickname);
    std::cout << "Login: "; std::getline(is, cont.login);
    std::cout << "Address: "; std::getline(is, cont.post_addr);
    std::cout << "E-mail: "; std::getline(is, cont.email);
    std::cout << "Phone number: "; std::getline(is, cont.phone);
    std::cout << "Birthday: "; std::getline(is, cont.birthday);
    std::cout << "Favorite meal: "; std::getline(is, cont.favor_meal);
    std::cout << "Underwear color: "; std::getline(is, cont.underwear_col);
    std::cout << "Darkest secret: "; std::getline(is, cont.secret);
    return is;
}
