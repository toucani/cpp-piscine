/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phonebook.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/02 20:00:23 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/03 08:11:16 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <iomanip>
#include "contact.hpp"

const unsigned contacts_am = 1;
contact contacts[contacts_am];
unsigned current = 0;

static void Add()
{
    if (current < contacts_am)
        std::cin >> contacts[current++];
    else
        std::cout << "Contact limit is reached" << std::endl;
}

static std::string GetShort(const std::string& item)
{
    if (item.length() < 10)
        return item;

    std::string result = item;
    result.erase(9);
    result.back() = '.';
    return result;
}

static void Search()
{
    std::cout << " -------------------------------------------" << std::endl;
    std::cout << "|   index  |first name| last name| nickname |" << std::endl;
    for (unsigned ct = 0; ct < current; ct++)
    {
        std::cout << '|' << std::setw(10) << std::setiosflags(std::ios_base::right) << ct;
        std::cout << '|' << std::setw(10) << std::setiosflags(std::ios_base::right) << GetShort(contacts[ct].first_name);
        std::cout << '|' << std::setw(10) << std::setiosflags(std::ios_base::right) << GetShort(contacts[ct].last_name);
        std::cout << '|' << std::setw(10) << std::setiosflags(std::ios_base::right) << GetShort(contacts[ct].nickname);
        std::cout << '|' << std::endl << std::resetiosflags(std::ios_base::right);
    }
    std::cout << std::endl;

    if (current == 0)
        return ;

    std::cout << "Type in a number to see more about corresponding contact: ";
    std::string temp;
    std::getline(std::cin, temp);
    try
    {
        const int id = std::stoi(temp);
        if (id >= 0 && id < int(current))
            std::cout << contacts[id];
    }
    catch(std::invalid_argument& ex)
    {
    }
}

int main(void)
{
    std::cout << "Type ADD to add a contact, SEARCH to look "
        "for a contact and EXIT to exit." << std::endl;

    for (std::string input; input != "EXIT";)
    {
        std::getline(std::cin, input);
        if (input == "ADD")
        {
            Add();
        }
        else if (input == "SEARCH")
        {
            Search();
        }
        else
        {
            std::cout << "Wrong command." << std::endl;
        }
        std::cout << "Done!" << std::endl;
    }

    return 0;
}
